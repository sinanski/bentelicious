const path = require('path');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');

const PATHS = {
  app: path.join(__dirname, 'src'),
  build: path.resolve(__dirname, 'dist'),
  image: '../assets/js-img'
};

console.log(__dirname)

module.exports = {
  entry: {
    app: './src/index.js',
    },
  plugins: [
      // new CleanWebpackPlugin(['dist/*']) for < v2 versions of CleanWebpackPlugin
      new CleanWebpackPlugin(),
      new HtmlWebpackPlugin({
        template: 'src/html/index.html',
        inject: false
      }),
      new HtmlWebpackPlugin({
        filename: 'about/index.html',
        template: 'src/html/about/index.html',
        inject: false
      }),
      new HtmlWebpackPlugin({
        filename: 'contact/index.html',
        template: 'src/html/contact/index.html',
        inject: false
      }),
      new HtmlWebpackPlugin({
        filename: 'policy/index.html',
        template: 'src/html/policy/index.html',
        inject: false
      }),
      new HtmlWebpackPlugin({
        filename: 'portfolio/index.html',
        template: 'src/html/portfolio/index.html',
        inject: false
      }),
  ],
  module: {
    rules: [
      {
        test: /\.(png|jpe?g|gif)$/i,
        loader: 'file-loader?name=/img/[name].[ext]',
        options: {
          name: '[name].[ext]',
          outputPath: 'assets/js-img',
          path: 'build/assets/js-img',
          file: 'bundle.js',
          publicPath: PATHS.image
        },
      },
      {
        test: /\.(js|jsx|mjs)$/,
        use: [
          { loader: 'babel-loader' },
        ],
        exclude: /node_modules/,
      },
      {
        test: /\.css$/i,
        use: ['style-loader', 'css-loader'],
      },
    ],
  },
  watch: true,
  devtool: 'cheap-module-eval-source-map',
  output: {
    filename: 'main.js',
    path: PATHS.build,
  },
};
