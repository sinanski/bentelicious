const path = require('path');
// var webpack = require('webpack');

const PATHS = {
  app: path.join(__dirname, 'src'),
  build: path.resolve(__dirname, 'dist')
};



module.exports = {
  entry: './src/index.js',
  output: {
    filename: 'main.js',
    path: PATHS.build,
  },
  module: {
    rules: [
      {
        test: /\.(png|jpe?g|gif)$/i,
        loader: 'file-loader',
        options: {
          outputPath: 'assets/js-img',
          publicPath: PATHS.build + '/assets/js-img'
        },
      },
      {
        test: /\.(js|jsx|mjs)$/,
        use: [
          { loader: 'babel-loader' },
        ],
        exclude: /node_modules/,
        // options: { babelrc: true },
      },
    ],
  },
  watch: true,
  devtool: 'cheap-module-eval-source-map',
  // plugins: [
  //   new webpack.HotModuleReplacementPlugin(),
  // ]
};

