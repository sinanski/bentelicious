import $ from 'jquery';
import {gsap} from 'gsap';
import TextPlugin from './gsap/TextPlugin.min';
import MotionPathPlugin from './gsap/MotionPathPlugin';
import User from './user';
import PersonaStyle from './persona-style';
import Sequence from './sequence/Sequence';
import Portfolio from './portfolio';
import State from './state-handler';
import Navigation from "./navigation";
import './css/style.css';
// import './nav/nav';
import './contact';

new Navigation();

const state = new State();

window.ui = state;

gsap.registerPlugin(TextPlugin);
gsap.registerPlugin(MotionPathPlugin);


const user = new User();
window.user = user;

const sequence = new Sequence()
window.sequence = sequence;

new PersonaStyle().renderClasses();

const portfolio = new Portfolio();
window.portfolio = portfolio;

window.testTimeline = gsap.timeline()


// Remove original Dialog Container from Dom after loading
$('#dialog').remove()
