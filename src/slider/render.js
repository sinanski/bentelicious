import Shape from "../shapes";
import $ from "jquery";
import * as ui from './dom/dom_ui';

export function render(img, id) {
  const fullImage = createDomNode(img, id);
  $('#portfolio').append(fullImage)
  const cleanId = '#full_' + id.replace('.', '\\.');
  const width = $(cleanId).width();
  // $('#portfolio-full').css('width', width)
}

export function dom_full_image(img, id) {
  const cleanId = id.split('.')[0];
  return `
  <div
    id="full_${id}"
    class="portfolio-full-image"
  >
    <img
      src="${img}"
      alt="${id}"
      class="portfolio-full"
    >
    ${createBackground()}
  </div>
  `
}

function createDomNode(img, id) {
  return `
    <div id="slider" class="full-overlay full-screen-image">
          ${createUiElements()}
      <div id="bg-black" class="bg-black"></div>
      <div id="slider-image" class="image-full">
        <div
          id="portfolio-full"
          style="height: 100%;width: auto;"
        >
          ${dom_full_image(img, id)}
        </div>
      </div>
    </div>
  `
}

function createUiElements() {
  const closeButton = ui.dom_close();
  const arrowButton = ui.dom_arrow();
  const arrowLeftButton = ui.dom_left_arrow();
  return closeButton + arrowButton + arrowLeftButton;
}

function createBackground() {
  const shape = new Shape({width: 100, height: 100, className:"shape-parent full-img-bg"});
  const rectangle = shape.rectangle().randomize({x: 10, y: 7});
  const bg = rectangle.grow({x: 1, y: 1, fill: 'black'}).randomize({x: 5, y: 2})
  return shape
  .add(bg)
  .add(rectangle)
  .createDomObject();
}
