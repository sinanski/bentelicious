import {gsap} from "gsap";

export function timelineAppear(onReverseComplete, isSingle) {
  const arrows = !isSingle ? animateArrows() : noArrows();
  const tl = new gsap.timeline({
    paused: true,
    onReverseComplete,
  });
  tl
    .add(animateImage(), 0)
    .add(arrows, 0)
    .add(animateClose(), 0)
  return tl;
}

function animateImage() {
  const tl = new gsap.timeline()
  .fromTo('#portfolio-full', {
    opacity: 0,
    scale: .8
  }, {
    duration: .4,
    ease: "power2.inOut",
    opacity: 1,
    scale: 1
  }, 0)
  .fromTo('#bg-black', {
    opacity: 0,
  }, {
    duration: .3,
    ease: "power2.inOut",
    opacity: 1
  }, 0)
  return tl;
}

function noArrows() {
  const tl = new gsap.timeline()
  .to('#slider-play', {
    duration: 0,
    x: '-100%',
    opacity: 0,
  })
  .to('#slider-reverse', {
    duration: 0,
    x: '-100%',
    opacity: 0,
  })
  return tl;
}

function animateArrows() {
  const tl = new gsap.timeline()
  .fromTo('#slider-play', {
    x: '100%',
    opacity: 0,
    // zIndex: -1
  }, {
    duration: .5,
    delay: .1,
    opacity: 1,
    ease: "back.out(1.7)",
    x: 0,
    // zIndex: 1,
  }, 0)
  .fromTo('#slider-reverse', {
    x: '-100%',
    opacity: 0,
    // zIndex: -1
  }, {
    duration: .5,
    delay: .1,
    opacity: 1,
    ease: "back.out(1.7)",
    x: 0,
    // zIndex: 1,
  }, 0)
  return tl;
}

function animateClose() {
  const tl = new gsap.timeline()
  .from('#slider-close', {
    delay: .3,
    duration: .5,
    opacity: 0,
    ease: "power3.out",
    y: '-100%'
  }, 0);
  return tl;
}
