import {gsap} from "gsap";
import $ from "jquery";
import {id} from '../get';

export function animateNext(thisId, nextId, onComplete) {
  animate(
    thisId,
    nextId,
    onComplete,
    '-50%',
    '50%'
  )
}

export function animatePrevious(thisId, nextId, onComplete) {
  animate(
    thisId,
    nextId,
    onComplete,
    '50%',
    '-50%'
  )
}

function animate(thisId, nextId, onComplete, to, from) {
  const current = id(thisId);
  const next = id(nextId);
  const tl = new gsap.timeline({
    // onComplete: () => onComplete(thisId),
    defaults: {
      duration: .25,
      // ease: "circ.inOut",
    }
  });
  tl
  .to(current, {
    opacity: 0,
    ease: 'power2.out',
    x: to
  }, )
  .call(onComplete, [thisId])
  .from(next, {
    // position: 'absolute',
    opacity: 0,
    ease: 'power2.out',

    x: from
  }, )
}
