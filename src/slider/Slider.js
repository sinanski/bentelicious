import $ from "jquery";
import * as get from './get';
import { render, dom_full_image } from './render';
import { attachEventHandlers } from './attachEventHandlers';
import { timelineAppear } from "./animation/timelineAppear";
import { animateNext, animatePrevious } from './animation/sliderAnimation';

export default class Slider {
  constructor({
                images,
                id,
                onClose = () => {}
  }) {
    unmountDomNode = unmountDomNode.bind(this);
    this.onClose = onClose;
    this.boundEventHandlers = attachEventHandlers.bind(this);
    this.images = images;
    this.id = id;
    this.timelineAppear = null
  }
  render(images = this.images, id = this.id) {
    this.images = images;
    this.id = id;
    const img = images[id].default;
    render(img, id);
    this.boundEventHandlers();
    this.playIn();
    return this;
  }
  playIn() {
    const isSingle = Object.keys(this.images).length === 1;
    this.timelineAppear = timelineAppear(unmountDomNode, isSingle).play()
  }
  play() {
    const next = get.next(this.images, this.id)
    renderNext(this.images, next);
    animateNext(this.id, next, unmountImage);
    this.id = next;
  }
  reverse() {
    const next = get.previous(this.images, this.id)
    renderNext(this.images, next);
    animatePrevious(this.id, next, unmountImage);
    this.id = next;
  }
  unmount() {
    this.timelineAppear.timeScale(1.5).reverse()
  }
}

function unmountImage(id) {
  $(get.id(id)).remove()
}

function renderNext(images, id) {
  const image = images[id].default;
  const domNode = dom_full_image(image, id);
  $('#portfolio-full').append(domNode)
}

function unmountDomNode() {
  $('#slider').remove();
  this.onClose(this.id)
}
