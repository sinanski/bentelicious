import $ from "jquery";

export function attachEventHandlers() {
  $('#slider-close').on('click', () => {
    this.unmount()
  })
  $('#slider-play').on('click', () => {
    this.play()
  })
  $('#slider-reverse').on('click', () => {
    this.reverse()
  })
}
