export function id(id) {
  return '#full_' + id.replace('.', '\\.');
}

export function next(images, id) {
  const keys = Object.keys(images)
  const currentIndex = keys.indexOf(id);
  const next = currentIndex < keys.length - 1
    ? currentIndex + 1
    : 0;
  return keys[next];
}

export function previous(images, id) {
  const keys = Object.keys(images)
  const currentIndex = keys.indexOf(id);
  const next = currentIndex <= 0
    ? keys.length - 1
    : currentIndex - 1;
  return keys[next];
}
