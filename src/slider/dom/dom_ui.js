import closeIcon from "../../img/ui/X.png";
import arrowIcon from "../../img/ui/arrow.png";

export const dom_close = () => (`
  <button id="slider-close" class="btn close-button">
    <img src="${closeIcon}" alt="close" class="ui-icon">
  </button>
`);

export const dom_arrow = () => (`
  <button id="slider-play" class="btn arrow-button">
    <img src="${arrowIcon}" alt="close" class="ui-icon">
  </button>
`);

export const dom_left_arrow = () => (`
  <button id="slider-reverse" class="btn arrow-left-button">
    <img src="${arrowIcon}" alt="close" class="ui-icon">
  </button>
`);
