import ID from '../utils/idGenerator';
import { getDialogImage } from "../sequence/layout/dialog/getImage";
import { getChatImage } from "../sequence/layout/chat/getImage";

export default class User {
  constructor() {
    addLocation = addLocation.bind(this);
    if ( localStorage.user &&  JSON.parse(localStorage.user)) {
      const {
        id,
        name,
        gender,
        hasAgreedDsgvo,
        visitedPages
      } = JSON.parse(localStorage.user);
      this.id = id;
      this.name = name;
      this.gender = gender;
      this.hasAgreedDsgvo = hasAgreedDsgvo;
      this.visitedPages = visitedPages; // currently not counting
    } else {
      this.id = ID();
      this.name = '';
      this.gender = false;
      this.hasAgreedDsgvo = false;
      this.visitedPages = [];
      write(this)
    }
    addLocation()
  }
  getChatImage() {
    return getChatImage('dude')
  }
  getDialogImage() {
    return getImage(this.gender)
  }
  setState( newState = {} ) {
    for (let name in newState) {
      if ( this[name] !== undefined ) {
        this[name] = newState[name];
      } else {
        console.warn(`What the fuck should I do with ${name} within user`)
      }
    }
    write(this);
    return new User();
  }
  clear() {
    console.warn('User has been cleared')
    localStorage.clear()
  }
}

function addLocation() {
  const page = window.location.href;
  if ( this.visitedPages.indexOf(page) === -1 ) {
    this.setState({
      visitedPages: this.visitedPages.concat(page)
    })
  }
}

function getImage(gender) {
  return !gender || gender === 'diverse'
    ? getDialogImage('user')
    : gender === 'male'
      ? getDialogImage('user_male')
      : getDialogImage('user_female')
}

function write(user) {
  const userString = JSON.stringify(user);
  localStorage.setItem('user', userString)
}
