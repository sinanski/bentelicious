import $ from 'jquery';
import { speechBubble, wiggle } from "./config/speechBubbleShape";
import ID from '../utils/idGenerator';
import Shape from "./index";
import PersonaStyle from '../persona-style';


export default function SpeechBubble({
                                       className = 'speech-bubble-dialog',
  name = 'Bente'
} = {}) {
  this.id = ID();
  this.className = className;
  this.wiggle = wiggle;
  this.pauseWiggle = 'pause';
  this.name = name;
  this.createDomObject = () => speechBubble(this.className);
  this.render = id => render(id, this.wiggle, this.className);
  this.setName = (target, name) => setName(target, name)
}

function render(id, wiggle, className) {
  const bubble = speechBubble(className);
  $(id).append(bubble);
  createNameArrow();
  wiggle()
}

function createNameArrow() {
  const target = '#dialog-name';
  const shape = new Shape({
    target,
    className: 'mirror',
    width: 100,
    height: 20
  });
  const arrow = shape.arrow({fill: 'white', full: false});
  const background = arrow.grow({x: 30, y: 30, fill: 'black'});
  shape
  .add(background)
  .add(arrow)
  .render()
  .wiggle(8)
  invert(target)
}

function setName(target, name) {
  const $elem = $(target);
  invertLetter($elem, name)
}

function invert(target) {
  const $elem = $(target + ' .dialog-name__inner');
  const name = $elem.html()
  invertLetter($elem, name)
}

function invertLetter($elem, name) {
  // const $elem = $(target + ' .dialog-name__inner');
  const newName = new PersonaStyle().invertLetter(
    name,
    'black',
    36,
    40,
    'shape-parent top-8'
  );
  $elem.html(newName)
}
