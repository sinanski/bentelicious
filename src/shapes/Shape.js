import {gsap} from 'gsap';
import SVGShape from './SVGShape';
import registerShape from './config/registerShape';
import ID from '../utils/idGenerator';
import wiggle from './animation/wiggleShape';
import {create, render} from './render/renderShape';
import {
  Rectangle,
  SpeechBubble,
  ChatBubble,
  ChatAnswer,
  Path,
  Arrow,
  LeftArrow,
  Apostrophe
} from './config/shapeTypes';

export default class Shape {
  constructor({
      width = '100%',
      height = '100%',
      onHover = [],
      className = 'shape-parent',
      target = '#test-shape',
      preserveAspectRatio = 'none',
      viewBox = `0 0 100 100`,
      full,
      // viewBox = `0 0 ${width} ${height}`,
      id = ID()
    } = {}) {
    setShapeProps = setShapeProps.bind(this)
    setState = setState.bind(this)
    this.state = {
      width,
      height,
      viewBox,
      fill: 'white',
      full
    };
    this.children = [];
    this.className = className;
    this.id = id;
    this.onHover = onHover;
    this.target = target;
    this.timeline = new gsap.timeline({ repeat: -1 });
    this.preserveAspectRatio = preserveAspectRatio;
    this.setState = state => this.state = setState(state);
  }
  rectangle(config) {
    const props = configToProps(config, this);
    const rectangle = new Rectangle(props);
    this.setState(rectangle)
    return new SVGShape(rectangle)
  }
  speechBubble(config) {
    const props = configToProps(config, this);
    const bubble = new SpeechBubble(props)
    this.setState(bubble)
    return new SVGShape(bubble)
  }
  chatBubble(config) {
    const props = configToProps(config, this);
    const bubble = new ChatBubble(props)
    this.setState(bubble)
    return new SVGShape(bubble)
  }
  chatAnswer(config) {
    const props = configToProps(config, this);
    const bubble = new ChatAnswer(props)
    this.setState(bubble)
    return new SVGShape(bubble)
  }
  path(config) {
    const props = configToProps(config, this);
    const path = new Path(props)
    this.setState(path)
    return new SVGShape(path)
  }
  arrow(config = {}) {
    const props = configToProps(config, this);
    const arrow = new Arrow(props);
    this.setState(arrow)
    return new SVGShape(arrow)
  }
  arrowLeft(config={}) {
    const props = configToProps(config, this);
    const arrow = new LeftArrow(props);
    this.setState(arrow)
    return new SVGShape(arrow)
  }
  add(child) {
    this.children = [
      ...this.children,
      child
    ]
    return this;
  }
  wiggle(amount) {
    if ( typeof amount === 'number' ) {
      this.children.forEach(shape => {
        const tl = wiggle(shape, amount)
        this.timeline.add(tl, 0)
      })
    } else {
      console.warn('amount should be a number')
    }
    this.timeline.play()
  }
  pauseWiggle() {
    this.timeline.pause()
  }
  render() {
    registerShape(this)
    render(this)
    return this;
  }
  createDomObject() {
    // Hier ist vermutlich das Problem, dass das Object noch nciht gerendert
    // ist, wenn es sich registriert. Dadurch geht der Event Listener auf ein
    // noch nciht existierendes Objekt.
    // registerShape(this)
    return create(this)
  }
  register() {
    registerShape(this)
  }
}

function setState(newState) {
  return {
    ...this.state,
    ...newState
  }
}

function setShapeProps(shape) {
  shape.width ? this.width = shape.width : undefined;
  shape.height ? this.height = shape.height : undefined;
  shape.viewBox ? this.viewBox = shape.viewBox : undefined;
  shape.className ? this.className += ' ' + shape.className : undefined;
  shape.target ? this.target = shape.target : undefined;
}


function configToProps(config = {}, that) {
  return {
    width: config.width || that.state.width,
    height: config.height || that.state.height,
    viewBox: config.viewBox || that.state.viewBox,
    id: ID(),
    ...config
  }
}
