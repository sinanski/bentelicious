import {gsap} from "gsap";

export default function wiggle(elem, amount) {
  let tl = new gsap.timeline({defaults: {duration: .1}});
  const wiggle1 = wigglePoint(elem, amount);
  const wiggle2 = wigglePoint(elem, amount);
  const wiggle3 = wigglePoint(elem, amount);
  tl
  .to('#' + elem.id, {
    attr:{
      points: wiggle1
    }
  })
  .to('#' + elem.id, {
    attr:{
      points: wiggle2
    }
  })
  .to('#' + elem.id, {
    attr:{
      points: wiggle3
    }
  })
  .to('#' + elem.id, {
    attr:{
      points: elem.coordinates
    }
  });
  return tl
}

function wigglePoint(elem, amount = 5) {
  const { coordinates, displacementMap } = elem;
  return coordinates.map((point, i) => {
    const x = displacementMap[i][0] * amount;
    const y = displacementMap[i][1] * amount;
    return [
      randomize(point[0], x),
      randomize(point[1], y)
  ]})
}

function randomize(value, amount) {
  const multiplier = Math.random() > .5 ? 1 : -1;
  const rand = Math.round(Math.random() * (amount - 1) + 1);
  return value + rand * multiplier;
}
