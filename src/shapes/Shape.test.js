import Shape from "./Shape";
const shape = new Shape();

describe('New Rectangle coordinates are calculated successfully', () => {
  const expected = [[0, 100]];
  const rectangle = shape.rectangle()
  it('With no amount defined the coordinates are either 0 or 100', () => {
    expect(rectangle.coordinates).toEqual(expect.arrayContaining(expected));
  });
  it('Only numbers are included, no %', () => {
    expect(['100%']).not.toEqual(expect.arrayContaining(expected));
  });
  it('ViewBox only contains numbers', () => {
    expect(rectangle.viewBox).toEqual('0 0 100 100');
  });
});

describe('Rectangle has the right config', () => {
  test('Basic rectangle has white fill with opacity 1 and no stroke', () => {
    const rectangle = shape.rectangle();
    expect(rectangle).toHaveProperty('fill', 'white');
    expect(rectangle).toHaveProperty('fillOpacity', 1);
    expect(rectangle).toHaveProperty('stroke', 'none');
    expect(rectangle).toHaveProperty('strokeWidth', 0);
  });
  test('Configurated Shape has red fill with .5 opacity and a blue stroke with a width of 1', () => {
    const config = {fill: 'red', fillOpacity: .5, stroke: 'blue', strokeWidth: 1};
    const rectangle = shape.rectangle(config);
    expect(rectangle).toHaveProperty('fill', 'red');
    expect(rectangle).toHaveProperty('fillOpacity', .5);
    expect(rectangle).toHaveProperty('stroke', 'blue');
    expect(rectangle).toHaveProperty('strokeWidth', 1);
  })
});