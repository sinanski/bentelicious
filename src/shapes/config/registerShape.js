import $ from "jquery";
import {ids} from "../../sequence/render/ids";

export default function registerShape(that) {
  registerEventHandlers(that)
  if ( !window.shapeController ) {
    window.shapeController = {
      [that.id]: that
    }
  } else {
    window.shapeController = {
      ...window.shapeController,
      [that.id]: that
    }
  }
//  Hier registriert sich jeder Shap beim window Objekt. Das verwaltet dann vor allem das wiggle
}

function registerEventHandlers({ id, onHover }) {
  // Currently not possible to pass amount to values
  handleElementHover(id)
  // onHover.forEach(event => {
  //   handleElementHover(id, event)
  // })
}

function handleElementHover(id) {
  $('#' + id).on({
    mouseenter: function (e) {
      // Klappt noch nicht
      console.log('Trigger')

      handleHover(e).wiggle(7)
    },
    mouseleave: function (e) {
      handleHover(e).pauseWiggle(7)
    }
  });
}

function handleHover(e) {
  const $elem = $(e.target);
  const id = $elem.find('svg').attr('id');
  return window.shapeController[id]
}
