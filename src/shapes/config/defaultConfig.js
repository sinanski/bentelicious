const config = {
  shape: {
    className: 'shape'
  },
  path: {
    type: 'path',
    extend: true,
  },
  arrow: {
    extend: true,
    type: 'arrow',
  },
  rectangle: {
    extend: true,
    width: 100,
    height: 100,
    x: 55, // default amount for randomize
    y: 20,
    defaultGrow: {
      x: 10,
      y: 5
    },
    fill: 'black',
    displacementMap: [
      [-1, -1],
      [1, -1],
      [1, 1],
      [-1, 1],
    ]
  }
};

export default config;
