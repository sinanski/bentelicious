export {
  Rectangle,
  SpeechBubble,
  ChatBubble,
  ChatAnswer,
  Path,
  Arrow,
  LeftArrow,
  Apostrophe
};

function getPureNumber(num) {
  const numberOnly = /\d+/g;
  return typeof num === 'number'
    ? num
    : Number(num.match(numberOnly)[0]);
}

function Rectangle(options = {}) {
  const { width, height } = options;
  const pureWidth = getPureNumber(width);
  const pureHeight = getPureNumber(height);
  const basicCoordinates = [
    [0,0],
    [pureWidth, 0],
    [pureWidth, pureHeight],
    [0, pureHeight],
  ];
  return {
    ...options,
    coordinates: options.coordinates || basicCoordinates
  }
}


function SpeechBubble(options ={}) {
  const coordinates = [
    [170,95],
    [974,0],
    [1040,350],
    [230,281],
    [215,301],
    [144,248],
    [127,269],
    [0,172],
    [96,191],
    [92,146],
    [176,184]
  ];
  const displacementMap = [
    [-1,-1],
    [0,0],
    [0,0],
    [0,1],
    [.3,1],
    [0,1],
    [0,1],
    [-1,-.5],
    [-1,-1],
    [-1,-1],
    [-1,-1],
  ]
  const viewBox="0 0 1040 350";
  return {
    ...options,
    viewBox,
    coordinates,
    displacementMap
  }
}



function ChatBubble(options ={}) {
  const coordinates = [
    [177,18],
    [1040,0],
    [1017,350],
    [153,324],
    [162,212],
    [102,233],
    [78,175],
    [8,183],
    [96,130],
    [127,183],
    [165,168],

  ];
  const displacementMap = [
    [-1,-1.5],
    [3,-1],
    [1,1],
    [-1.5,1.5],
    [-1,1],
    [0,1],
    [0,1],
    [-1,.3],
    [0,-1],
    [0,-1],
    [-1,-1],
  ]
  const viewBox="0 0 1040 350";
  return {
    ...options,
    viewBox,
    coordinates,
    displacementMap
  }
}


function ChatAnswer(options ={}) {
  const coordinates = [
    [0,153],
    [129,700],
    [912,507],
    [850,250],
    [891,199],
    [946,224],
    [988,174],
    [832,190],
    [937,188],
    [877,136],
    [850,180],
    [789,0],

  ];
  const displacementMap = [
    [-3,-1],
    [-2,1],
    [1,1],
    [1,1],
    [1,1],
    [0,1],
    [1,0],
    [0,-1],
    [0,-1],
    [0,-2],
    [1,-1],
    [1,-1],
  ]
  const viewBox="0 0 988 700";
  return {
    ...options,
    viewBox,
    coordinates,
    displacementMap
  }
}

function Path(options = {}) {
  const {points, width, height} = options;
  const viewBox = `0 0 ${width} ${height}`;
  let coordinates = [];
  let displacementMap = [];
  for ( let i = 0; i < points; i++ ) {
    let x = 0;
    const isFirstOrLast = i === 0 || i === points - 1;
    const isLeft = i%2 === 0;
    switch(true) {
      case (isFirstOrLast) :
        x = width / 2;
        displacementMap.push([0,0]);
        break;
      case(isLeft) :
        x = width / 4;
        displacementMap.push([-1, 0]);
        break;
      default :
        x = width / 4 * 3;
        displacementMap.push([1, 0]);
    };
    const y = height / ( points -1 ) * i;
    coordinates.push([x, y])
  }
  return {
    type: 'path',
    strokeWidth: 5,
    stroke: 'black',
    ...options,
    displacementMap,
    viewBox,
    coordinates: options.coordinates || coordinates
  }
}

function Arrow(options = {}) {
  const viewBox="0 0 944 184";
  const coordinates = [
    [107, 0],
    [779, 17],
    [750, 85],
    [944, 131],
    [700, 184],
    [716, 131],
    [0, 184]
  ];
  const displacementMap = [
    [-1, -1],
    [1, -1],
    [1, -1],
    [2, 0],
    [-1, 1],
    [-1, 1],
    [-1, 1],
  ];
  return {
    ...options,
    width: '100%',
    height: '100%',
    viewBox,
    coordinates,
    displacementMap
  }
}

function LeftArrow(options) {
  // Not working with width 100%
  const arrow = new Arrow(options)
  const { coordinates, displacementMap, width } = arrow;
  const newCoords = coordinates.map( coords => ([
    width - coords[0],
    coords[1]
  ]));
  const newMap = displacementMap.map( coords => ([
    coords[0] * -1,
    coords[1]
  ]));
  return {
    ...arrow,
    coordinates: newCoords,
    displacementMap: newMap
  }
}

function Apostrophe(options) {

}
