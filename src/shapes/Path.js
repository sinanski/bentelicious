import ID from "../utils/idGenerator";
import render from "./render/renderPath";

export default class Path {
  constructor({
                width = 200,
                height = 140,
                points = 4,
                className = 'shape-parent',
                target = '#test-shape',
                preserveAspectRatio = 'none',
                strokeWidth = 5,
                stroke = 'black',
                viewBox = `0 0 ${width} ${height}`,
                id = ID()
              }) {
    this.children = [];
    this.className = className;
    this.id = id;
    this.target = target;
    this.state = {
      width,
      height,
      viewBox,
      stroke,
      strokeWidth,
      points,
      coordinates: [],
      displacementMap: []
    };
    const path = PathOptions(this.state);
    this.setState(path)
    // console.log(this.state)
  }
  setState(newState) {
    this.state = {
      ...this.state,
      ...newState
    }
  }
  addPoint(config = {}) {
    // const
    // console.log(config.distance)
    const { points, height } = this.state;
    const path = PathOptions({
      ...this.state,
      points: points + 1,
      height: height + height / (points - 1),
      right: config.right,
      left: config.left,
      distance: config.distance
    });
    this.setState(path)
    this.render()
  }
  render() {
    render(this)
    return this;
  }
}

function configToProps(config = {}, that) {
  return {
    width: config.width || that.state.width,
    height: config.height || that.state.height,
    viewBox: config.viewBox || that.state.viewBox,
    points: config.points || that.state.points,
    id: ID(),
    ...config
  }
}


function PathOptions(options = {}) {
  const {points, width, height, left, right, distance} = options;
  const viewBox = `0 0 ${width} ${height}`;
  let coordinates = options.coordinates || [];
  let displacementMap = options.displacementMap || [];
  for ( let i = coordinates.length; i < points; i++ ) {
    let x = 0;
    // const isFirstOrLast = i === 0 || i === points - 1;
    const isLeft = i%2 === 0;
    switch(true) {
      // case (isFirstOrLast) :
      //   x = width / 2;
      //   displacementMap.push([0,0]);
      //   break;
      case(!!left) :
        x = typeof left === 'number' ? left : 0;
        displacementMap.push([-1, 0]);
        break;
      case(right) :

        x = width;
        displacementMap.push([-1, 0]);
        break;
      case(!!distance) :
        const length = coordinates.length - 1;
        const thisPoint = coordinates[length];
        const lastPoint = coordinates[length - 1];
        const multiplier = thisPoint[0] > lastPoint[0] ? 1 : - 1;
        // console.log('lastPoint', lastPoint[0])
        // console.log('thisPoint', thisPoint[0])
        // console.log('distance', distance)
        x = thisPoint[0] - distance * multiplier;
        displacementMap.push([-1, 0]);
        break;

      case(isLeft) :
        x = width / 4;
        displacementMap.push([-1, 0]);
        break;
      default :
        x = width / 4 * 3;
        displacementMap.push([1, 0]);
    }
    const y = height / ( points -1 ) * i;
    // coordinates = coordinates.concat([x, y])
    coordinates.push([x, y])
  }
  return {
    type: 'path',
    ...options,
    displacementMap,
    viewBox,
    coordinates: coordinates
  }
}
