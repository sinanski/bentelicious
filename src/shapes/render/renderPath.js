import $ from 'jquery'

export default function render({
  id,
  target,
  className,
  children,
  preserveAspectRatio = 'none',
  state,
}) {
  const {
    width,
    height,
    viewBox,
    stroke,
    strokeWidth,
    points,
    coordinates,
  } = state;
  const path = renderPathClean(id, state);
  const childNodes = path;
  // const childNodes = renderChildren(children);
  const parent = renderParent({width, height, className, viewBox, id, preserveAspectRatio}, childNodes)
  renderInPlace(id, target, parent)
};

function renderInPlace(id, target, child) {
  const $elem = target ? $(target) : $('body');
  const currentChild = $elem.find('#' + id)
  const hasElement = !!currentChild.length;
  if ( hasElement ) {
    currentChild.replaceWith(child)
  } else {
    $elem.append(child)
  }
}

function renderParent({width, height, className, viewBox, id, preserveAspectRatio}, childNodes) {
  // Width und Height sind nur für die Demo wichtig.
  // Später sollten die raus und die Größe richtet sich nach dem Parent
  const svg = `
    <svg
      id="${id}"
      class="${className} overflow-visible"
      width="${width}"
      height="${height}"
      viewBox="${viewBox}"
      preserveAspectRatio="${preserveAspectRatio}"
    >
        ${childNodes}
    </svg>
  `;
  return svg
}

function renderPathClean(id, state) {
  return createPath({id, ...state});
}

function createPath({
  id,
  coordinates,
  stroke,
  strokeWidth
                    }) {
  const points = getPathCoords(coordinates);
  return `
    <path
      id="${'id'}"
      stroke-width="${strokeWidth}"
      stroke="${stroke}"
      stroke-linejoin="bevel"
      fill="none"
      d="${points}"
    >  
  </path>
  `;
}

function getPathCoords(coords) {
  return coords.map(( single, i ) => {
    let prefix = i === 0 ? 'M' : 'L';
    return [
      prefix + single[0],
      single[1]
    ]
  })
}


// function createPolygon({
//   coordinates,
//   fill = 'white',
//   stroke,
//   strokeWidth,
//   fillOpacity,
//   id,
// }) {
//   const points = coordinates.join(' ');
//   return `
//   <polygon
//     class="persona-background-fill"
//     id="${id}"
//     fill="${fill}"
//     stroke-linejoin="crop"
//
//     fill-opacity="${fillOpacity}"
//     stroke="${stroke}"
//     stroke-width="${strokeWidth}"
//     points="${points}">
//   </polygon>
//   `
// }
