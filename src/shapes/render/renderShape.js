import $ from 'jquery'


export function create(props) {
  const childNodes = renderChildren(props.children)
  if ( props.state.full ) {
    return renderParentFull(props, childNodes)
  }
  return renderParent(props, childNodes)
}

export function render(_this) {
  const domNode = create(_this)
  const $elem = _this.target ? $(_this.target) : $('body');
  $elem.append(domNode)
}

function renderParentFull({ className, id }, childNodes) {
  const svg = `
    <svg
      id="${id}"
      class="${className} overflow-visible"
      width="100%"
      height="100%"
      preserveAspectRatio="none"
    >
        ${childNodes}
    </svg>
  `;
  return svg
}

function renderParent({
                        state,
                        className,
                        id,
                        preserveAspectRatio
                      }, childNodes) {
  const {
    width,
    height,
    viewBox
  } = state;
  const svg = `
    <svg
      id="${id}"
      class="${className} overflow-visible"
      width="${width}"
      height="${height}"
      viewBox="${viewBox}"
      preserveAspectRatio="${preserveAspectRatio}"
    >
        ${childNodes}
    </svg>
  `;
  return svg
}

function renderChildren(children) {
  const domNode = children.map( shape => (
    switchShape(shape)
  )).join('')
  return children.length > 1
    ? `<g>${domNode}</g>`
    : domNode
}

function switchShape(shape) {
  switch(shape.type) {
    case 'rectangle' :
      return createPolygon({...shape});
    case 'path' :
      return createPath(shape);
    default:
      console.warn(shape.type + ' not known')
  }
}

function createPath({
  id,
  coordinates,
  stroke,
  strokeWidth
                    }) {
  const points = getPathCoords(coordinates);
  return `
    <path
      id="${'id'}"
      stroke-width="${strokeWidth}"
      stroke="${stroke}"
      stroke-linejoin="bevel"
      fill="none"
      d="${points}"
    >  
  </path>
  `;
}

function getPathCoords(coords) {
  return coords.map(( single, i ) => {
    let prefix = i === 0 ? 'M' : 'L';
    return [
      prefix + single[0],
      single[1]
    ]
  })
}


function createPolygon({
  coordinates,
  fill = 'white',
  stroke,
  strokeWidth,
  fillOpacity,
  id,
}) {
  const points = coordinates.join(' ');
  return `
  <polygon
    class="persona-background-fill"
    id="${id}"
    fill="${fill}"
    stroke-linejoin="crop"
    fill-opacity="${fillOpacity}"
    stroke="${stroke}"
    stroke-width="${strokeWidth}"
    points="${points}">
  </polygon>
  `
}
