import $ from 'jquery'

export function create({
                           id,
                           target,
                           className,
                           children,
                           preserveAspectRatio,
                           state,
                         }) {
  const {
    width,
    height,
    viewBox
  } = state;
  const childNodes = renderChildren(children)
  const domNode = renderParent({width, height, className, viewBox, id, preserveAspectRatio}, childNodes)
  return domNode
}


export function render(_this) {
  console.log(_this)
  const domNode = create(_this)
  const $elem = _this.target ? $(_this.target) : $('body')
  $elem.append(domNode)
}

// function renderParent({className, id, preserveAspectRatio = 'none'}, childNodes) {
//   // Width und Height sind nur für die Demo wichtig.
//   // Später sollten die raus und die Größe richtet sich nach dem Parent
//   const svg = `
//     <svg
//       id="${id}"
//       class="${className} overflow-visible"
//       width="100%"
//       height="100%"
//       preserveAspectRatio="${preserveAspectRatio}"
//     >
//         ${childNodes}
//     </svg>
//   `;
//   return svg
// }

function renderParent({width, height, className, viewBox, id, preserveAspectRatio, full}, childNodes) {
  // Width und Height sind nur für die Demo wichtig.
  // Später sollten die raus und die Größe richtet sich nach dem Parent
  console.log(full)
  const svg = `
    <svg
      id="${id}"
      class="${className} overflow-visible"
      width="${width}"
      height="${height}"
      viewBox="${viewBox}"
      preserveAspectRatio="${preserveAspectRatio}"
    >
        ${childNodes}
    </svg>
  `;
  return svg
}

function renderChildren(children) {
  const domNode = children.map( shape => (
    switchShape(shape)
  )).join('')
  return children.length > 1
    ? `<g>${domNode}</g>`
    : domNode
}

function switchShape(shape) {
  switch(shape.type) {
    case 'rectangle' :
      return createPolygon({...shape});
    case 'path' :
      return createPath(shape);
    default:
      console.warn(shape.type + ' not known')
  }
}

function createPath({
  id,
  coordinates,
  stroke,
  strokeWidth
                    }) {
  const points = getPathCoords(coordinates);
  return `
    <path
      id="${'id'}"
      stroke-width="${strokeWidth}"
      stroke="${stroke}"
      stroke-linejoin="bevel"
      fill="none"
      d="${points}"
    >  
  </path>
  `;
}

function getPathCoords(coords) {
  return coords.map(( single, i ) => {
    let prefix = i === 0 ? 'M' : 'L';
    return [
      prefix + single[0],
      single[1]
    ]
  })
}


function createPolygon({
  coordinates,
  fill = 'white',
  stroke,
  strokeWidth,
  fillOpacity,
  id,
}) {
  const points = coordinates.join(' ');
  return `
  <polygon
    class="persona-background-fill"
    id="${id}"
    fill="${fill}"
    stroke-linejoin="crop"

    fill-opacity="${fillOpacity}"
    stroke="${stroke}"
    stroke-width="${strokeWidth}"
    points="${points}">
  </polygon>
  `
}
