export default function displaceHandler(options, that) {
  const newCoords = that.coordinates.map( coord => {
    const x = typeof options.x === 'number' ? options.x : -10;
    const y = typeof options.y === 'number' ? options.y : -10;
    return ([
      coord[0] + x,
      coord[1] + y
    ]
  )});
  return {
    ...that,
    ...options,
    coordinates: newCoords,
  }
};
