import defaultConfig from '../config/defaultConfig';

export default function grow(options, that) {
  const { defaultGrow } = defaultConfig.rectangle;
  const props = {
    x: typeof options.x === 'number' ? options.x : defaultGrow.x,
    y: typeof options.y === 'number' ? options.y : defaultGrow.y,
    // x: options.x || defaultGrow.x,
    // y: options.y || defaultGrow.y,
    fill: options.fill || 'white',
    stroke: options.stroke,
    strokeWidth: options.strokeWidth
  };
  const newCoords = growShape({...props, ...that})
  return {
    ...that,
    ...props,
    coordinates: newCoords,
  }
  // return new SVGShape(
  //   new Rectangle({
  //     ...that,
  //     ...props,
  //     coordinates: newCoords,
  //   }))
}

function growShape({
   x,
   y,
   coordinates,
   displacementMap
  }) {
  return coordinates.map(( point, i ) => {
    const newX = point[0] + x * displacementMap[i][0];
    const newY = point[1] + y * displacementMap[i][1];
    return [ newX, newY ]
  })
}
