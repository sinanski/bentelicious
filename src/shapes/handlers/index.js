import randomize from './randomize';
import grow from './grow';
import displace from './displace';

const handler = {
  randomize,
  grow,
  displace,
}

export default handler;
