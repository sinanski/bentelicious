import defaultConfig from '../config/defaultConfig';

export default function randomizeHandler(options, that) {
  const mergedOptions = {
    ...defaultConfig[that.type],
    coordinates: options.coordinates || that.coordinates,
    displacementMap: that.displacementMap || defaultConfig.rectangle.displacementMap,
    ...options
  };
  const coordinates = randomizeCoordinates(mergedOptions);
  return {
    ...that,
    coordinates
  }
};

function randomizeCoordinates({
  coordinates,
  displacementMap,
  extend,
  x,
  y,
}) {
  const newMap = extend
    ? displacementMap
    : calculateDisplacementMap(displacementMap);
  return coordinates.map(( coord, i ) => {
    const newX = randomizePosition(coord[0], newMap[i][0], x);
    const newY = randomizePosition(coord[1], newMap[i][1], y);
    return [ newX, newY ]
  })
}

function randomizePosition(coord, displacement, amount = 1) {
  return coord + random(amount) * displacement;
}


function calculateDisplacementMap(displacementMap) {
  return displacementMap.map(point => (
    point.map(() => (
      Math.random() > .5 ? 1 : -1
    ))
  ))
}

function random(maxVal) {
  return Math.round(Math.random() * ( maxVal - 1 ) + 1)
}
