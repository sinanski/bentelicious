import defaultConfig from './config/defaultConfig';
import handle from './handlers';
import { Rectangle } from './config/shapeTypes';
import ID from '../utils/idGenerator';


export default class SVGShape {
  constructor({
    // id,
    width,
    height,
    type = 'rectangle',
    displacementMap,
    coordinates,
    fill = 'white',
    fillOpacity = 1,
    stroke = 'none',
    strokeWidth = 0,
    viewBox,
    points,
  } = {}) {
    this.id = ID();//id;
    this.point = points;
    this.width = width;
    this.height = height;
    this.type = type;
    this.coordinates = coordinates;
    this.fill = fill;
    this.fillOpacity = fillOpacity;
    this.stroke = stroke;
    this.strokeWidth = strokeWidth;
    this.viewBox = viewBox;
    this.displacementMap = displacementMap
      ? displacementMap
      : defaultConfig.rectangle.displacementMap
  }
  randomize(options = {}) {
    const props = handle.randomize(options, this)
    return new SVGShape(
      new Rectangle(props)
    );
  }
  grow(options = {}) {
    const props = handle.grow(options, this)
    return new SVGShape(
      new Rectangle(props)
    );
  }
  displace(options = {}) {
    const props = handle.displace(options, this);
    return new SVGShape(
      new Rectangle(props)
    );
  }
}
