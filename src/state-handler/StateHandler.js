import $ from 'jquery';
const $html = $('html');
import * as render from './render';

export default class StateHandler {
  constructor() {
    onPortraitMode = onPortraitMode.bind(this);
    onLandscapeMode = onLandscapeMode.bind(this);
    this.state = {
      mode: '',
      hasHover: false,
      forceLandscape: false,
      onModeChange: [],
      menuOpen: false,
  };
    // Bei aktivem FoecreLandscape wird das Dreh icon eingeblendet sobald
    //Portrait aktiviert wird
    setScreenMode();
    checkHover(() => this.setState({ hasHover: true }));
  }
  setState(newState) {
    this.state = {
      ...this.state,
      ...newState
    }
  }
}


const setScreenMode = () => {
  const width = window.innerWidth;
  const height = window.innerHeight;
  if ( height > width ) {
    onPortraitMode()
  }
  if ( width > height ) {
    onLandscapeMode()
  }
}

function checkHover(onHasHover) {
  window.addEventListener('mouseover', function onFirstHover() {
    onHasHover()
    window.removeEventListener('mouseover', onFirstHover, false);
  }, false);
}

function onPortraitMode() {
  if ( this.state.mode !== 'portrait' ) {
    this.setState({mode: 'portrait'});
    if (this.state.forceLandscape) {
      console.log('Show Overlay')
      render.forceLandscape();
    }
    $html.addClass('portrait-mode')
    $html.removeClass('landscape-mode')
  }
}

function onLandscapeMode() {
  if ( this.state.mode !== 'landscape' ) {
    render.removeforceLandscape();
    this.setState({mode: 'landscape'});
    $html.addClass('landscape-mode')
    $html.removeClass('portrait-mode')
  }
}
$(window).on('resize', () => setScreenMode());
