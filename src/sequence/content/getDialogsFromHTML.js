import $ from "jquery";
import Dialog from "./Dialog";

export default function getDialogsFromHTML(id = '#dialog') {
  const $dialogs = getChildren(id);
  return mapDialogList($dialogs)
}

function mapDialogList($dialogs) {
  let dialogList = [];
  $dialogs.each(( i, dialogs ) => {
    const $dialogs = $(dialogs)
    const screens = getSingleDialog(dialogs);
    const source = 'HTML';
    const data = getData($dialogs)
    const type = getType($dialogs)
    const dialog = new Dialog({ screens, source, data, type })
    dialogList.push(dialog)
  });
  return dialogList;
}

function getSingleDialog(dialogs) {
  const children = getChildren(dialogs);
  return mapDialogs(children).toArray()
}

function mapDialogs($dialog) {
  return $dialog.map((i, child) => (
    handleDialogChildren(child)
  ))
}

function handleDialogChildren(child) {
  const hasChildren = getChildren(child).length;
  if ( hasChildren ) {
    return mapChildren(child)
  } else {
    return createKeyValuePair(child)
  }
}

function mapChildren(child) {
  const children = getChildren(child);
  const $child = $(child);
  const label = getData($child).label;
  return {
    type: getType($child),
    content: mapDialogs(children).toArray(),
    id: label,
    label
  }
}

function createKeyValuePair(element) {
  const $elem = $(element);
  const data = getData($elem);
  return {
    data,
    target: data.target,
    id: data.label,
    label: data.label,
    text: getText($elem),
    type: getType($elem)
  }
}

function getChildren(id) {
  return $(id).children();
}

function getText($elem) {
  return $elem.text().trim();
}

function getData($elem) {
  return $elem.data();
}

function getType($elem) {
  return $elem.prop("tagName");
}