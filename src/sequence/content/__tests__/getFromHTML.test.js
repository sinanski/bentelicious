'use strict';
import getDialogsFromHTML from "../getDialogsFromHTML";
import mockHTML from "../mockFiles/mockHTML";
import mockJSON from "../mockFiles/mockJSON";


test('HTML dialog is read and converted to JSON', () => {
  document.body.innerHTML = mockHTML;
  const result = getDialogsFromHTML()
  expect(result.length).toBe(2)
  expect(result[0]).toMatchObject(mockJSON[0])
})