import Dialog from "../Dialog";
import gender from "../../global-dialogs/gender";

const dialog = new Dialog(gender);

describe('Gender Dialog is converted as it should be', () => {

  test('Dialog contains all necessary content', () => {
    expect(dialog).toEqual(
      expect.objectContaining({
        id: 'gender',
        label: 'gender',
        source: 'GLOBAL',
        type: 'TEXT',
        priority: expect.any(String),
        screens: expect.arrayContaining([
          expect.objectContaining({
            type: expect.any(String),
            id: expect.any(String),
            data: expect.any(Object),
          })
        ])
      })
    )
  })

  test('Gender Dialog contains 3 screens', () => {
    expect(dialog.screens.length).toBe(3)
  })

  test('Gender Dialog contains an answer with 3 choices', () => {
    const answer = dialog.screens.filter(({ type }) => {
      return type === 'ANSWER'
    })[0];
    expect(answer.content.length).toBe(3)
  })
})
