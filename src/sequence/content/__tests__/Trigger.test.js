import Trigger from "../Trigger";
import Dialog from "../Dialog";
import gender from "../../global-dialogs/gender";
import dsgvo from "../../global-dialogs/DSGVO";



test('Trigger works for Gender Dialog', () => {
  const dialog = new Dialog(gender);
  const trigger = new Trigger(dialog);
  const expected = {
    id: 'gender',
    priority: 0,
    delay: 0,
    in: 'auto',
    out: undefined,
  }
  expect(trigger).toMatchObject(expected);
  expect(trigger).toEqual(
    expect.objectContaining({
      onComplete: expect.any(Function),
      onClick: expect.any(Function),
    })
  )
});

test('Trigger works for DSGVO Dialog', () => {
  const dialog = new Dialog(dsgvo);
  const trigger = new Trigger(dialog);
  const expected = {
    id: 'dsgvo',
    priority: 2,
    delay: 0,
    in: 'auto',
    out: undefined,
  }
  expect(trigger).toMatchObject(expected);
  expect(trigger).toEqual(
    expect.objectContaining({
      onComplete: expect.any(Function),
      onClick: expect.any(Function),
    })
  )
});
