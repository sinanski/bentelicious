import PageEvent from "../PageEvent";
import gender from "../../global-dialogs/gender";
import {user, userForGender} from '../mockFiles/user'

test('New Event creates an event with dialog and trigger out of JSON Dialog', () => {
  const event = new PageEvent({
    dialog: gender,
    user: userForGender
  });
  expect(event).toEqual(
    expect.objectContaining({
      dialog: expect.objectContaining({
        id: 'gender',
      }),
      trigger: expect.objectContaining({
        id: 'gender',
        in: 'auto',
        priority: 0
      }),
      isActive: true
    })
  )
});

test('Gender event is not active if only 2 pages are visited', () => {
  const event = new PageEvent({
    dialog: gender,
    user: user
  });
  expect(event).toHaveProperty('isActive', false)
})

test('Gender event is active if at least 3 pages are visited and no gender exists in user object', () => {
  const event = new PageEvent({
    dialog: gender,
    user: userForGender
  });
  expect(event).toHaveProperty('isActive', true)
})

test('If "isNeeded" within dialog is undefined the dialog is automatically active', () => {
  const dialog = {
    ...gender,
    isNeeded: undefined,
  }
  const event = new PageEvent({
    dialog,
    user: userForGender
  });
  expect(event).toHaveProperty('isActive', true)
})

// test('New Event works with array of strings in screens', () => {
//   // Not working yet!!
//   const screens = ['Hello', 'World']
//   const event = new Event({screens}, userForGender);
//   console.log(event.dialogs[0].screens)
// })