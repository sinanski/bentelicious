import ID from "../../utils/idGenerator";

export default function Dialog({
                                 id = ID(),
                                 screens,
                                 source = 'GLOBAL',
                                 data = {},
                                 type = 'TEXT',
                                 priority = 'normal',
}) {
  this.id = id;
  this.screens = addId(screens);
  this.source = source;
  this.data = convertData(data);
  this.type = type;
  this.priority = priority;
  this.label = data.label;
  this.image = data.image;
}

function addId(screens) {
  return screens.map(screen => addIdToScreens(screen));
}

function addIdToScreens(screen) {
  return {
    ...screen,
    id: screen.id || ID(),
    content: addIdToContent(screen.content),
    data: screen.data || {}
  }
}

function addIdToContent(content) {
  if ( !content ) return;
  return content.map( single => ({
    ...single,
    id: ID()
  }));
}

function convertData(data) {
  let cleanData = {};
  for ( let key in data ) {
    const cleanKey = key.replace('trigger', '');
    cleanData[cleanKey] = data[key];
  }
  return cleanData;
}
