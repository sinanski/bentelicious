const mock = {
  "dialogs": [
  {
    "id": "_mk3v4zrb4",
    "image": "bente",
    "type": "DIALOG",
    "screens": [
      {
        "id": "_s4f4ge98b",
        "data": {},
        "type": "TEXT",
        "text": "He na, du hier."
      },
      {
        "id": "_en3wwgwz0",
        "data": {
          "image": "bente_smirk"
        },
        "type": "TEXT",
        "text": "Was suchst du hier?"
      },
      {
        "id": "_wqmavjfv7",
        "data": {},
        "type": "ANSWER",
        "content": [
          {
            "id": "_lf1xwxrin",
            "data": {
              "target": "good"
            },
            "type": "CHOICE",
            "text": "Die besten Bilder der Welt"
          },
          {
            "id": "_7i0uusla3",
            "data": {
              "target": "bad"
            },
            "type": "CHOICE",
            "text": "Nur mal durchgucken."
          }
        ]
      },
      {
        "id": "good",
        "label": "good",
        "data": {
          "target": "break",
          "image": "bente_happy",
          "label": "good"
        },
        "type": "TEXT",
        "text": "DANN bist du hier genau richtig! Klick mal oben auf das Burger-Menü ;o)"
      },
      {
        "id": "bad",
        "label": "bad",
        "data": {
          "label": "bad"
        },
        "type": "TEXT",
        "text": "CHATBOT ACTIVATE"
      },
      // {
      //   "id": "_8a8ix6yqv",
      //   "data": {},
      //   "type": "TEXT",
      //   "text": "Wenn du zu den Nacktaktiven Fledermäusen willst, sag bitte laut  NACKTAKTIVE FLEDERMÄUSE"
      // },
      // {
      //   "id": "_nlh9lmg01",
      //   "data": {},
      //   "type": "TEXT",
      //   "text": "Wenn du mein Portfolio sehen willst, dann drücke drei mal Shift."
      // },
      // {
      //   "id": "_ovjagrud5",
      //   "data": {},
      //   "type": "TEXT",
      //   "text": "Wenn du mir eine Nachricht schreiben willst, dann geh zu Googlemail, wie jeder vernünftige Boomer."
      // },
      // {
      //   "id": "_d6bye57ss",
      //   "data": {},
      //   "type": "ANSWER",
      //   "content": [
      //     {
      //       "id": "_tofyxe6qs",
      //       "data": {},
      //       "type": "CHOICE",
      //       "text": "Nacktaktive Fledermäuse"
      //     },
      //     {
      //       "id": "_j96dtjhqv",
      //       "data": {},
      //       "type": "CHOICE",
      //       "text": "Portfolio"
      //     },
      //     {
      //       "id": "_wr2rmohx8",
      //       "data": {},
      //       "type": "CHOICE",
      //       "text": "Kontaktformular A38"
      //     }
      //   ]
      // },
      // {
      //   "id": "_vx0km8gox",
      //   "data": {},
      //   "type": "TEXT",
      //   "text": "Ok, rufe \"Mama Festnetz\" an. Ist das richtig?"
      // },
      // {
      //   "id": "_x1bgi6igd",
      //   "data": {},
      //   "type": "ANSWER",
      //   "content": [
      //     {
      //       "id": "_6wpcndfbt",
      //       "data": {},
      //       "type": "CHOICE",
      //       "text": "Ich hasse Chatbots"
      //     }
      //   ]
      // },
      {
        "id": "_o5pbra613",
        "data": {},
        "type": "TEXT",
        "text": "Ich bin kein Chatbot! ICH BIN DER BENTE BOT! Und jetzt drücke oben auf das Burgermenü und find dich selbst zurecht."
      }
    ]
  },
  {
    "id": "_cd6gjrso5",
    "image": "bente_smirk",
    "type": "DIALOG",
    "screens": [
      {
        "id": "_c990x7c8q",
        "data": {},
        "type": "TEXT",
        "text": "Das mit dem Burger Menü hast du schon kapiert, oder?"
      },
      {
        "id": "_3tl1xlrgb",
        "data": {},
        "type": "TEXT",
        "text": "Mehr passiert hier nicht mehr."
      }
    ]
  },
  {
    "image": "../assets/js-img/dsgvo-bulle.png",
    "label": "dsgvo",
    "id": "dsgvo",
    "screens": [
      {
        "id": "_1onq4fsk4",
        "text": "Hier spricht die Keks-Polizei.",
        "type": "TEXT"
      },
      {
        "id": "_9dx9m83q9",
        "text": "HÄNDE HOCH ODER ICH SCHLIEßE!",
        "type": "TEXT"
      },
      {
        "id": "_rv7nc3bk7",
        "text": "Hier werden Cookies und Analytics verwendet.",
        "type": "TEXT"
      },
      {
        "id": "_uyl0acy3k",
        "type": "ANSWER",
        "content": [
          {
            "type": "CHOICE",
            "text": "Schon klar",
            "data": {
              "target": "break"
            }
          },
          {
            "type": "CHOICE",
            "text": "Ich will's genau"
          }
        ]
      },
      {
        "id": "_bw7kzlsww",
        "text": "Du willst es also genau wissen. Vorbildlich!",
        "type": "TEXT",
        "data": {}
      }
    ]
  }
],
  "trigger": [
  {
    "id": "_mk3v4zrb4",
    "priority": 0,
    "delay": 0,
    "in": "auto",
    "out": "toggle-menu"
  },
  {
    "id": "_cd6gjrso5",
    "priority": 0,
    "delay": 4,
    "in": "auto",
    "out": "toggle-menu"
  },
  {
    "id": "dsgvo",
    "priority": 2,
    "delay": 0,
    "in": "auto"
  }
]
}

export default mock;