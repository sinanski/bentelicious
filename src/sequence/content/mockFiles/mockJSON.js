const mockJSON = [
  {
    screens: [
      {
        text: 'He na, du hier.',
        type: 'TEXT'
      },
      {
        text: 'Was suchst du hier?',
        type: 'TEXT',
        data: {
          image: 'bente_smirk'
        }
      },
      {
        type: 'ANSWER',
        content: [
          {
            type: 'CHOICE',
            text: 'Die besten Bilder der Welt',
            data: {
              target: 'good'
            }
          },
          {
            type: 'CHOICE',
            text: 'Nur mal durchgucken.',
            data: {
              target: 'bad'
            }
          },
        ]
      },
      {
        id: "good",
        label: "good",
        text: 'DANN bist du hier genau richtig! Klick mal oben auf das Burger-Menü ;o)',
        type: 'TEXT',
        data: {
          target: "break",
          image: "bente_happy",
          label: "good"
        }
      },
      {
        id: "bad",
        label: "bad",
        text: 'CHATBOT ACTIVATE',
        type: 'TEXT',
        data: {
          image: "bente_bot",
          label: "bad"
        }
      },
      {
        text: 'Ich bin kein Chatbot! ICH BIN DER BENTE BOT! Und jetzt drücke oben auf das Burgermenü und find dich selbst zurecht.',
        type: 'TEXT',
      },
    ]
  },
  {
    data: {
      image: 'bente_smirk',
      delay: 4
    },
    screens: [
      {
        text: 'Das mit dem Burger Menü hast du schon kapiert, oder?',
        type: 'TEXT'
      },
      {
        text: 'Mehr passiert hier nicht mehr.',
        type: 'TEXT'
      },
    ]
  }
];

export default mockJSON