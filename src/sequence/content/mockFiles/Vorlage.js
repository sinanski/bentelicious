const content = {
  // dialogs and trigger allways need to have the same length.
  // Each dialog has a matching trigger with matching id
  dialogs: [
    {
      id: "_lbgzkpi42",
      label: 'bad',
      image: "bente",
      type: "DIALOG",
      screens: [
        {
          id: "_swrgustr8",
          label: undefined,
          data: {
            target: "break",
            image: "bente_happy",
            label: "good"
          },
          type: "TEXT",
          text: "He na, du hier."
        }
      ]
    },
    {
      id: "_a75494on6",
      label: undefined,
      data: {},
      type: "ANSWER",
      content: [
        {
          id: "_n1b75gz4c",
          label: undefined,
          data: {},
          type: "CHOICE",
          text: "Ich hasse Chatbots"
        },
        {
          id: "_n1b75gz4b",
          label: undefined,
          data: {
            target: 'better'
          },
          type: "CHOICE",
          text: "Ich finds toll"
        }
      ]
    }
  ],
  trigger: [
    {
      id: "_lbgzkpi42",
      priority: 0,
      delay: 0,
      in: "auto",
      out: "toggle-menu",
      onComplete: undefined,
      onClick: () => {}
    }
  ]
};