import EventCollector from "../EventCollector";
import {user, userForGender} from "./user";
// import dsgvo from "../../global-dialogs/DSGVO";
// import gender from "../../global-dialogs/gender";
import getDialogsFromHTML from "../getDialogsFromHTML";
import mockHTML from "./mockHTML";
// import mockOldDialogs from "../mockFiles/mockOldDialogs";

test('New Events match old Events! (Kill Test after code shift)', () => {
  document.body.innerHTML = mockHTML;
  const htmlResult = getDialogsFromHTML()
  const events = new EventCollector(user, htmlResult);
  // console.log(events.dialogs)

  // expect(events.dialogs).toMatchObject(mockOldDialogs.dialogs);
  // expect(events.trigger).toMatchObject(mockOldDialogs.trigger);

});

test('Dialogs match the expected structure. Containing type, id, image, screens', () => {
  document.body.innerHTML = mockHTML;
  const htmlResult = getDialogsFromHTML()
  const events = new EventCollector(user, htmlResult);
  expect(events.dialogs).toEqual(
    expect.arrayContaining([
      expect.objectContaining({
        type: 'DIALOG',
        id: expect.any(String),
        image: expect.any(String),
        screens: expect.arrayContaining([
          expect.objectContaining({
            type: expect.any(String),
            id: expect.any(String),
            data: expect.any(Object),
            content: expect.any(Array),
          })
        ])
      })
    ])
  )
})