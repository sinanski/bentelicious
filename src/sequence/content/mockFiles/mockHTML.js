const mockHTML = `
  <section id="dialog">
    <dialog
      data-type="DIALOG"
      data-triggerIn="auto"
      data-triggerDelay="0"
      data-triggerOut="toggle-menu"
      data-image="bente"
    >
      <text>
        He na, du hier.
      </text>
      <text data-image="bente_smirk">
        Was suchst du hier?
      </text>
      <answer>
        <choice data-target="good">
          Die besten Bilder der Welt
        </choice>
        <choice data-target="bad">
          Nur mal durchgucken.
        </choice>

      </answer>
      <text
        data-label="good"
        data-image="bente_happy"
        data-target="break"
      >
        DANN bist du hier genau richtig! Klick mal oben auf das Burger-Menü ;o)
      </text>
      <text
        data-label="bad"
        data-image="bente_bot"
      >
        CHATBOT ACTIVATE
      </text>
      
      <text>
        Ich bin kein Chatbot! ICH BIN DER BENTE BOT! Und jetzt drücke oben auf das Burgermenü und find dich selbst zurecht.
      </text>
    </dialog>
    <dialog
      data-type="DIALOG"
      data-triggerIn="auto"
      data-triggerDelay="4"
      data-triggerOut="toggle-menu"
      data-image="bente_smirk"
    >
      <text>Das mit dem Burger Menü hast du schon kapiert, oder?</text>
      <text>Mehr passiert hier nicht mehr.</text>
    </dialog>
  </section>
`;

export default mockHTML;