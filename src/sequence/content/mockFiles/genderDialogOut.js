const genderDialog = {
  "label": "gender",
  "id": "gender",
  "screens": [
  {
    "id": "_wn9nlom1g",
    "text": "Sag mal was bist du eigentlich?",
    "type": "TEXT"
  },
  {
    "id": "_dbyh3el63",
    "type": "ANSWER",
    "content": [
      {
        "type": "CHOICE",
        "text": "Frau",
        "data": {}
      },
      {
        "type": "CHOICE",
        "text": "Mann",
        "data": {}
      },
      {
        "type": "CHOICE",
        "text": "Divers",
        "data": {}
      }
    ]
  },
  {
    "id": "_0blfhnehr",
    "text": "Ach so",
    "type": "TEXT"
  }
]
}

export default genderDialog;