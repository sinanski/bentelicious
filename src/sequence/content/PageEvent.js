import Dialog from "./Dialog";
import Trigger from "./Trigger";

export default function PageEvent({
                                dialog,
                                user = window.user,
}) {
  this.dialog = new Dialog(dialog);
  this.trigger = new Trigger(this.dialog);
  this.isActive = checkNeed(user, dialog)
}

function checkNeed(user, dialog) {
  const { isNeeded} = dialog;
  if ( Array.isArray(isNeeded) ) {
    return handleArrayOfNeeds(user, isNeeded)
  } else {
    return checkIfActive(user, isNeeded)
  }
}

function handleArrayOfNeeds(user, isNeeded) {
  let result = true;
  isNeeded.forEach( needs => {
    if ( !checkIfActive(user, needs) ) {
      result = false
    }
  });
  return result;
}

function checkIfActive(user = {}, needs = {}) {
  const { key, value, type = 'equal' } = needs;
  const isDone = type === 'length'
    ? (user[key] || []).length < value
    : user[key] !== value;
  return !isDone;
}