export default function Trigger({
  id,
  priority,
  data = {},
  onClick = () => {},
  onComplete = () => {},
} = {}) {
  if(!id) {
    // console.error('Ohne ID passiert hier irgendwann ein Fehler')
    return {}
  }
  this.id = id;
  this.priority = getPriority(priority);
  this.delay = data.delay || 0;
  this.in = data.in || 'auto';
  this.out = data.out;
  this.type = data.type;
  this.onComplete = onComplete;
  this.onClick = onClick;
}

function getPriority(priority) {
  const priorities = [
    'normal',
    'high',
    'highest',
  ];
  return priorities.indexOf( priority )
}