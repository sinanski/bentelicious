import getDialogsFromHTML from "./getDialogsFromHTML";
import global from "../global-dialogs";
import PageEvent from "./PageEvent";

export default function EventCollector() {
  const fromPage = getDialogsFromHTML();
  this.pageEvents = fromPage;
  this.globalEvents = global;
  const eventList = getEventList(fromPage);
  this.dialogs = getDialogs(eventList);
  this.trigger = getTrigger(eventList);
  this.inactive = getInactiveEvents(eventList);
}

function getEventList(fromPage) {
  const merged = [...fromPage, ...global];
  return merged.map(event => (
    new PageEvent({dialog:event})
  ));
}

function getActiveEvents(eventList) {
  return eventList.filter(event => event.isActive);
}

function getInactiveEvents(eventList) {
  return eventList.filter(event => !event.isActive);
}

function getDialogs(eventList) {
  const activeEvents = getActiveEvents(eventList)
  return activeEvents.map(({dialog}) => dialog);
}

function getTrigger(eventList) {
  const activeEvents = getActiveEvents(eventList)
  return activeEvents.map(({trigger}) => trigger);
}
