import $ from 'jquery';
import { gsap } from 'gsap';
import dom_flash from './dom_flash';


export default class Flash {
  constructor() {
  }
  render() {
    $('#scene').append(dom_flash())
    $('#flash').on('click', function() {
      console.log('Click')
      console.log($(this))
      $(this).css('visibility', 'hidden')
      // animateIn().reverse()
    })
    return this;
  }
  playIn(text) {
    animateIn(text).play()
  }
  playOut() {
    animateIn().reverse()
  }

}


function animateIn(text) {
  let tl = new gsap.timeline({paused: true});
  tl
  .set('#flash', {visibility: 'visible'})
  .to('#flash .flash-text', {duration: .1, text:{ value:text }})
  .fromTo('#flash', {
    duration: .3,
    opacity: 0,
  }, {
    opacity: 1
  }, 0)
  .fromTo('#flash .flash-image', {
    // duration: .15,
    x: '100%',
    y: '-100%',
    ease: "power3.in"
  }, {
    duration: .3,
    x: 0,
    y: 0,
  }, 0)
  return tl;
}
