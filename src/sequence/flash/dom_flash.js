import marlene from '../../img/flash/marlene-flash.png';
import Shape from '../../shapes';

export default function dom_flash(text = 'Placeholder') {
  return `
  <div id="flash" class="full-overlay flash" style="visibility: hidden">
    <img src="${marlene}" alt="flash-marlene" class="flash-image">
    ${textBubble(text)}
  </div>
  `
}

function textBubble(text) {
  const shape = new Shape({className: ''});
  const rectangle = shape
    // Hier läuft was sehr schief. Fill und fillOpacity übertragen sich auch auf den Chat
    .rectangle({width: 400, height: 120, fill: 'black', fillOpacity: .85})
    .randomize({x: 35, y: 35});
  const background = rectangle
    .grow({fill: 'white'})
    .randomize({x: 8, y: 8});
  shape
    .add(background)
    .add(rectangle)
  const bubble = shape.createDomObject()
  return `
    <div class="flash-speech-container">
      <div class="flash-speech">
        <div class="flash-text">
          ${text}
        </div>
        <div>
          ${bubble}
        </div>
      </div>
    </div>
  `
}
