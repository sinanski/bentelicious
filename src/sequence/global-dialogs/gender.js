import $ from 'jquery';
import { gsap } from 'gsap';
// import img from '../../img/dialog/bente-moody.png';
// import { getDialogImage } from '../../sequence/layout/dialog/getImage';

const gender = {
  id: 'gender',
  data: {
    // target: "end",
    // image: img,
    label: "gender",
  },
  screens: [
    {
      text: "Sag mal was bist du eigentlich?",
      type: "TEXT"
    },
    {
      type: 'ANSWER',
      content: [
        {
          type: 'CHOICE',
          text: 'Frau',
          data: {
            subScene: () => subScene('female')
          }
        },
        {
          type: 'CHOICE',
          text: 'Mann',
          data: {
            subScene: () => subScene('male'),
          }
        },
        {
          type: 'CHOICE',
          text: 'Divers',
          data: {
            subScene: () => subScene('diverse'),
          }
        },
      ],
    },
    {
      text: "Ach so",
      type: "TEXT"
    },
  ],
  isNeeded: [
    {
      key: 'gender',
      value: false
    },
    {
      key: 'visitedPages',
      value: 3,
      type: 'length',
    }
  ]
}



// async function subScene() {
//   console.log(window.user);
//   let tl = new gsap.timeline({onComplete: () => {return true} });
//   tl.to('#answer', {
//     duration: 2,
//     x: 400,
//   })
//   return await tl;
//   // return await timeout(1000);
// }

function subScene(gender) {
  const tl = new gsap.timeline()
  console.log('Hier soll die Animation ablaufen und dann wird auf einen Click auf window gewartet')
  return new Promise(async resolve => {
    window.user.setState({ gender })
    const test = await tl
    .set('#answer-options .dialog-choice', {
      visibility: 'hidden'
    })
    .to('#answer-user', {
      duration: .5,
      y: '100%',
      scaleX: .5,
      ease: "power3.in",
    })
    .set('#answer-user', {
      attr: { src: window.user.getDialogImage()},
    })
    .to('#answer-user', {
      delay: 1,
      duration: .5,
      y: 0,
      scaleX: 1,
      ease: "power4.out"
    })
    .then(() => {
      $('#answer').on('click', () => {
        resolve();
      })
    })
  });
}

function timeout(ms) {
  return new Promise(resolve => setTimeout(resolve, ms));
}

export default gender;
