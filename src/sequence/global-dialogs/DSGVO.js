import img from '../../img/dialog/dsgvo-bulle.png';

const dsgvo = {
  id: 'dsgvo',
  priority: 'highest',
  data: {
    // target: "end",
    image: img,
    label: "dsgvo"
  },
  isNeeded: {
    key: 'hasAgreedDsgvo',
    value: false
  },
  screens: [
    {
      text: "Hier spricht die Keks-Polizei.",
      type: "TEXT",
    },
    {
      text: "HÄNDE HOCH ODER ICH SCHLIEßE!",
      type: "TEXT",
    },
    {
      text: "Hier werden Cookies und Analytics verwendet.",
      type: "TEXT",
    },
    {
      type: 'ANSWER',
      content: [
        {
          type: 'CHOICE',
          text: 'Schon klar',
          data: {
            target: 'break',
            subScene: () => window.user.setState({
              hasAgreedDsgvo: true
            })
          }
        },
        {
          type: 'CHOICE',
          text: "Ich will's genau",
        },
      ],
    },
    {
      text: "Du willst es also genau wissen. Vorbildlich!",
      type: "TEXT",
      data: {
        // target: 'break',
        subScene: () => setTimeout(() => {
          window.location = 'policy'
        }, 1000)
      }
    },
  ]
};

export default dsgvo;
