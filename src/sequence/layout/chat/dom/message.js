import DOMavatar from './avatar';
import DOMtext from './text';

export default function message({domId, image, text}) {
  const id = domId.split('#')[1]
  return `
  <div id="${id}" class="avatar" style="visibility: hidden;">
    ${DOMavatar(image)}
    ${DOMtext(text)}
  </div>
  `
}
