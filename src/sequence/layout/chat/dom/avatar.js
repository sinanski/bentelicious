import {ids} from '../../../render/ids';
import benteImg from '../../../../img/chat/bente.png';
import benteBg from '../../../../img/chat/bg-bente.png';
import bentorImg from '../../../../img/chat/bentor.png';
import marleneImg from '../../../../img/chat/marlene.png';
import marleneBg from '../../../../img/chat/bg-marlene.png';
import dudeImg from '../../../../img/chat/dude.png';
import dudeBg from '../../../../img/chat/bg-dude.png';

const images = {
  bente: {
    character: benteImg,
    background: benteBg
  },
  bentor: {
    character: bentorImg,
    background: benteBg
  },
  marlene: {
    character: marleneImg,
    background: marleneBg
  },
  dude: {
    character: dudeImg,
    background: dudeBg
  },
};

function getImage(name) {
  const current = images[name]
  return current
    ? current
    : images.bente
}

export default function avatar(img) {
  const image = getImage(img)
  return `
  <div class="avatar-img">
    <img src="${image.background}" class="img-fluid avatar-bg" alt="Bente dialog" />
    <img src="${image.character}" class="img-fluid avatar-img" alt="Bente dialog" />
  </div>
  `
}
