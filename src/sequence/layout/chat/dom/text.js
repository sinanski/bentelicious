import Shape from '../../../../shapes'

export default function text(text) {
  return `
  <div class="message-text">
    <p>${text}</p>
  </div>
  `
}
