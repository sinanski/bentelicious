
export function answer(id, answerList) {
  const parentId = 'answer' + id;
  const options = 'answer-options' + id;
  const answers = singleAnswer(answerList)
  return `
  <div id="${parentId}" class="chat-content">
    <div id="${id}" class="chat-answer" style="visibility: hidden;">
      <ul id="${options}">
        ${answers.join('')}
      </ul>
    </div>
  </div>
  `
}

export function answerOut(text, image) {
  return `
  <div class="avatar-img">
    <img
      src="${image.background}"
      class="img-fluid avatar-bg"
      alt="Bente dialog"
    >
    <img
      src="${image.character}"
      class="img-fluid avatar-img"
      alt="Bente dialog"
    >
  </div>
  <div class="message-text">
    <p>
      ${text}
    </p>
  </div>
  `
}

function singleAnswer(answerList) {
  return answerList.map((answer, i) => (
    `
      <li class="dialog-choice" style="pointer-events: initial;">
        <p class="answer-p">
          ${answer.text.trim()}
        </p>
      </li>
    `
  ))
}
