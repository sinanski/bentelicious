// import defaultAvatarImg from '../../../../img/chat/bente.png';
// import defaultBgImg from '../../../../img/chat/bg-bente.png';
import defaultAvatarImg from '../../../../img/chat/marlene.png';
import defaultBgImg from '../../../../img/chat/bg-marlene.png';
import { dom } from '../../../render/ids';
const {
  dialog,
  source,
} = dom();

import mobileImage from '../../../../img/dialog/mobile-frame.png';

export default function chat(image, text, id) {
  const defaultImage = defaultAvatarImg;
  const defaultBg = defaultBgImg;
  return `
    <div id="${dialog.container}" class="chat-container" style="visibility: visible">
      <img src="${mobileImage}" class="mobile-frame" alt="handy frame bentelicious">
      <div id="${source}" class="chat-screen">
      <div id="${dom().chat.content}" class="chat-content">
        <div id="chat-path"></div>
        
        </div>
      </div>
    </div>
  `;
}
