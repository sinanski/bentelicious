import {gsap} from "gsap";
import {ids} from "../../../render/ids";
import $ from "jquery";


export function dialogIn(delay = 0) {
  let tl = new gsap.timeline({delay});
  tl.fromTo(ids.dialog.container, {
    duration: .5,
    ease: "back.out(1.7)",
    y: '100%'
  }, {
    y: 0
  })
  return tl
}

export function dialogOut(onComplete) {
  let tl = new gsap.timeline({onComplete});
  tl.to(ids.dialog.container, {
    duration: .5,
    ease: "back.in(1.7)",
    y: '100%'
  })
  return tl
}

export function answerIn(id, domId, distance) {
  let subTl = new gsap.timeline();
  subTl
  .set(id, {visibility: 'visible'})
  .to(ids.chat.content, {duration: .25, y: distance}, 0)
  .to(domId, {
    motionPath:{
      start: 1,
      stop: 1,
      path: "#id",
      offsetX: -250,
      offsetY: -40,
    }}, 0)
  .from(domId, {
    duration: .5,
    ease: "back.out(1.7)",
    opacity: 0,
    scaleY: .5,
  }, 0)
  return subTl;
}

export function answerOut(domId) {
  let subTl = new gsap.timeline();
  // subTl.to(domId, {
  //   motionPath:{
  //     start: 1,
  //     stop: 1,
  //     path: "#id",
  //     offsetX: -250,
  //     offsetY: -40,
  //   }}, -.1)
  return subTl
}

export function updateAnswer(id, content) {
  let tl = new gsap.timeline()
  return tl;
}

function renderChatText(text) {
  $('#avatar').append('<div>'+ text + '</div>')
}


export function placeAvatar(domId, text, image) {
  // console.log(text, image);
  const bg = domId + ' .avatar-bg';
  const img = domId + ' .avatar-img';
  renderChatText(text)
  let tl = new gsap.timeline();
  tl
  .set(domId, {visibility: 'visible'})
  .set(img, {scale: 0})
  .set(bg, {scaleY: 0})
  .to(domId, {
    immediateRender: true,
    duration: .5,
    ease: "power1.inOut",
    motionPath:{
      start: 1,
      stop: 1,
      path: "#id",
      offsetX: -20,
      offsetY: -40,
    }
  }, 0)
  .to(bg, {
    duration: .15,
    ease: "back.out(2)",
    scaleY: 1
  }, 0)
  .to(img, {
    duration: .5,
    ease: "back.out(2)",
    scale: 1
  }, 0)
  .addPause();
  return tl;
}

export function nextChatScreen({domId, text, image}, distance) {
  let tl = new gsap.timeline();
  tl
  .to(ids.chat.content, {duration: .25, y: distance})
  .to(domId, {duration: .1}, -.1)
  .call(placeAvatar, [domId, text, image], 0)
  .from(domId + ' .message-text', {
    duration: .3,
    ease: "back.out(1.7)",
    opacity: 0,
    scaleY: .5,
    y: 30
  })
    // .add(animateDots())
  .to(domId, {duration: .1})
  return tl;
}

export function animateDots() {
  let tl = new gsap.timeline({repeat: -1})
  .to('#dots .dots-single', {
    duration: .4,
    opacity: 0,
    stagger: .15,
  })
  .to('#dots .dots-single', {
    duration: .4,
    opacity: 1,
    stagger: .3,
  })
  .to('#dots .dots-single', {
    duration: .3,
    opacity: 1,
  });

  return tl;
}
