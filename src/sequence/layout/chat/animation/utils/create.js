import $ from "jquery";
import Shape from "../../../../../shapes";

export function answerHighlight(id) {
  const options = $('#answer-options' + id + ' .dialog-choice');
  options.each((i, option) => {
    const $option = $(option);
    const height = $option.outerHeight();
    const width = $option.outerWidth();
    const shape = new Shape({
      onHover: ['wiggle', 'show'],
      width,
      height,
      className:'shape-parent z-1 show-on-hover'
    });
    const rectangle = shape.rectangle({full: true});
    shape.add(rectangle)
    const domNode = shape.createDomObject();
    $option.append(domNode)
    shape.wiggle(10)
    shape.register();
  })
}

export function answerBubble(id) {
  const $id = $('#' + id);
  const shape = new Shape({preserveAspectRatio: 'xMaxYMin', className: 'answer-bubble'});
  const answer = shape.chatAnswer();
  const bg = answer.grow({x: 10, y: 10, fill: 'black'})
  shape.add(bg)
  shape.add(answer)
  const domNode = shape.createDomObject();
  $id.append(domNode)
  // shape.render()
}

export function speechBubble(id) {
  const shape = new Shape({ target: id });
  const bubble = shape.chatBubble();
  const bg = bubble.grow({x: 20, y: 20, fill: 'black'}) //.wiggle(10);
  shape.add(bg);
  shape.add(bubble);
  shape.render()
}

export function dots() {
  const shape = new Shape({
    target: '#dots',
    width: 50,
    height: 25
  });
  const rectangle = shape
    .rectangle({fill: 'black', fillOpacity: .5, full: true })
    .randomize({x: 30, y: 10})

  shape.add(rectangle)
  const domDots = mapDots();
  const domNode = shape.createDomObject();
  $('#dots')
    .append(domNode)
    .append(domDots)
}

function mapDots() {
  let domDots = '';
  for ( let i = 1; i <= 3; i++ ) {
    domDots += singleDot(i)
  }
  return domDots;
}

const singleDot = i => (
  `
  <div>
    <svg id="single-dot-${i}" class="dots-single" viewBox="0 0 30 30">
      <circle
      cx="15" 
      cy="15" 
      r="50%" 
      preserveAspectRatio="xMinYMin meet" 
      fill="#f124c9" 
      />
    </svg>
  </div>
  `
)
