import {find} from "lodash";
import {ids} from "../../../../render/ids";

const fromBottom = 200; // This number changes the margin from the bottom of the screens. Stupidly it is not in px
const defaultDistance = 60;


const initialDistance = defaultDistance * -2;

export function domId(id) {
  return '#answer' + id;
}

export function current(screens, id) {
  const current = find( screens, {id} )
  const currentLabel = find( screens, {label: id} )
  return current || currentLabel;
}

export function scrollDistance(i) {
  let distance = i * defaultDistance * - 2 + fromBottom;
  return i < 2 || initialDistance < distance
    ? initialDistance
    : distance
}
