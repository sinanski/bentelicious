import { find } from 'lodash';
import * as animation from './chat-animation';
import {gsap} from "gsap";
// import {ids} from "../../../render/ids";
// import $ from "jquery";
import Chat from './ChatAnimator';


export function dialogInOut(id, delay, onComplete) {
  const tl = new gsap.timeline({ onComplete, delay });
  tl.addLabel(id)
  .add(animation.dialogIn(), 'in')
  .addPause()
  .add(animation.dialogOut(), 'out');
  return tl;
}

export function dialogUpdate(id, screens, image) {
  // return new Chat(screens, image)
}


// export function dialogUpdate(id, screens) {
//   const tl = new gsap.timeline({ paused: true });
//   screens.forEach(({
//                      type,
//                      text,
//                      label,
//                      id
//                    }) => {
//     if ( type === 'TEXT' ) {
//       tl.add(
//         animation.updateDialog(text, id, screens),label || id
//       )
//       tl.addPause()
//     }
//   });
//   return tl
// }

export function createAnswerInOut() {
  const tl = new gsap.timeline({paused: true});
  tl.add(animation.answerIn(), 'in')
  .addPause()
  .add(animation.answerOut(), 'out')
  .addLabel('is out')
  return tl;
}

export function createUpdateAnswer(id, screens) {
  const tl = new gsap.timeline({ paused: true });
  screens.forEach(({
                     type,
                     content,
                     label,
                     id
                   }) => {
    if ( type === 'ANSWER' ) {
      tl.add(
        animation.updateAnswer(id, content),label || id
      )
      tl.addPause()
    }
  });
  return tl
}
