import { findIndex } from 'lodash';
import * as get from './utils/get';
import * as create from './utils/create';
import {dom, ids} from "../../../render/ids";
import * as animation from "./chat-animation";
import $ from "jquery";
import Path from "../../../../shapes/Path";
import message from '../dom/message';
import { answer, answerOut } from '../dom/answer';

const distance = 60;

export default class Chat {
  constructor({
                screens,
                onComplete,
                image,
                data,
                delay
  },
              renderer,
  ) {
    this.screens = screens;
    this.delay = delay;
    this.image = image;
    this.data = data;
    this.renderer = renderer;
    this.onComplete = onComplete;
    this.avatar_wrapper = dom().chat.content;
    this.path = new Path({
      target: '#chat-path',
      id:"chat-path-inner",
      height: distance * 2,
      width: 400,
      points: 2,
      stroke: 'black',
      strokeWidth: 25,
    })
    this.state = {
      wasAnswer: false,
    }
    this.i = 1;
    // this.path = this.shape.path({points: 5, width: 200, height: 200, strokeWidth: 5});
  }
  setState(newState) {
    this.state = {
      ...this.state,
      ...newState
    }
  }
  init() {
    this.path.render()
    this.addPoint({left: 40})
    this.play(this.screens[0].id)
    animation.dialogIn(this.delay)
  }
  // Wenn es einen weiteren Dialog gibt soll ... eingeblendet werden
  play(id) {
    // Hier sollte noch eine Abfrage rein, die die nächste ID in der Liste
    // als Backup gibt. Damit die Funktion in window aufgerufen werden
    // kann. So läßt sich eine Vorschau besser testen
    const scrollDistance = get.scrollDistance(this.i)
    this.i ++;
    const current = this.getCurrent(id)
    this.renderMessage(current)
    const showDots = this.shouldRenderDots(id);
    if ( showDots ) {
      this.renderDots('#avatar' + id)
      // animation.animateDots()
    } else {
      removeDots()
    }
    animation.nextChatScreen(current, scrollDistance)
  }
  addPoint(config) {
    if ( !this.state.wasAnswer ) {
      this.path.addPoint(config)
    } else {
      this.path.addPoint({left: 40})
    }
  }
  updateDialog(label) {
    // Warum auch immer wird das hier bei einer Antwort doppelt getriggert
    // console.log('update Dialog', label)
    this.addPoint({distance: 20})
    this.setState({ wasAnswer: false })
    this.play(label)
  }
  answerIn(id) {
    removeDots()
    const scrollDistance = get.scrollDistance(this.i);
    this.i ++;
    // const scrollDistance = this.i * distance * - 2;
    const current = this.getCurrent(id);
    this.renderAnswer(current)
    this.addPoint({right: true})
    const domId = get.domId(id)
    animation.answerIn('#' + id, domId, scrollDistance)
    this.setState({ wasAnswer: true })
  }
  answerOut(id, content) {
    // const current = this.getCurrent(id);
    const image = window.user.getChatImage('user');
    const domObject = answerOut(content.text, image);
    const domId = get.domId(id)
    $(domId)
      .empty()
      .removeClass('chat-content')
      .addClass('avatar flex-row-reverse avatar-reverse')
      .append(domObject)

    create.speechBubble(domId + ' .message-text')
    $(domId + ' svg').addClass('mirror')
    animation.answerOut('#' + id, domId)
    // this.renderDots('#answer' + id)
  }

  getCurrent(id) {
    const current = get.current(this.screens, id);
    const image = current.data.image || this.image;
    const domId = ids.chat.avatar +  id;
    return {
      ...current,
      image,
      domId
    }
  }
  shouldRenderDots(id) {
    const index = findIndex(this.screens, {id});
    const isLast = index === this.screens.length - 1;
    if ( isLast ) {
      return false;
    }
    const target = this.getCurrent(id).data.target;
    if ( target ) {
      if ( target === 'break' ) {
        return false;
      }
      const next = this.getCurrent(target);
      if ( next.type === 'ANSWER' ) {
        return false;
      }
    } else {
      const next = this.screens[index + 1];
      if ( next.type === 'ANSWER' ) {
        return false;
      }
    }
    return true;
  }
  renderMessage(current) {
    const id = current.domId + ' .message-text';
    const domMessage = message(current)
    $('#' + this.avatar_wrapper)
      .append(domMessage)
    create.speechBubble(id)
  }
  renderAnswer(current) {
    // Scheiß label / ID macht wieder Probleme
    // Muss überall im Dokument noch ersetzt werden
    const id = current.label || current.id;
    const domMessage = answer(id, current.content)
    $('#' + this.avatar_wrapper)
    .append(domMessage)
    create.answerBubble(id);
    create.answerHighlight(id);
    this.renderer.attachEventHandlers({
      id,
      onChatAnswer: (i) => {} //console.log('clicked', i)
    })
  }
  renderDots(target) {
    const domMessage = `<div id="dots" class="dots-container"></div>`;
    removeDots();
    $(target)
    .append(domMessage)
    create.dots(id, this.screens)
    animation.animateDots()
  }
  playIn() {
    console.log('Play in')
    animation.dialogIn()
  }
  playOut() {
    animation.dialogOut()
  }
  end() {
    animation.dialogOut(this.onComplete);
  }
}

function removeDots() {
  $('#dots').remove();
}


