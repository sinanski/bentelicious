const defaultContent = [
  {text: 'Platzhalter'},
  {text: 'Platzhalter'},
  {text: 'Platzhalter'},
]

export default function answerChoice() {
  let domNode = '';
  defaultContent.forEach(( choice, i ) => {
    const singleChoice = domChoice(choice.text);
    domNode += singleChoice;
  });
  return domNode
}

const domChoice = (text) => (
  `
  <li class="dialog-choice" style="visibility: hidden">
    <div class="dialog-inner">
      <p class="dialog-text">
        ${text}
      </p>
    </div>
  </li>
  `
);
