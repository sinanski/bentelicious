import $ from "jquery";
import {ids} from "../../../render/ids";
import Shape from "../../../../shapes";
import answerChoice from './DOM_answerChoice';
import answerContainer from './DOM_answerContainer';

export default function Answer() {
  const image = window.user.getDialogImage();
  this.container = answerContainer(image);
  this.choice = answerChoice()
  this.renderArrow = () => renderAnswerBackground()
}

function renderAnswerBackground() {
  let $answer = $(ids.answer.choices);
  $answer.children().each(( i, answer ) => {
    createShape(answer)
  });
}

function createShape(answer) {
  const shape = new Shape({target: answer, className: 'answer-arrow'});
  const arrow = shape
  .arrow({ fill: 'black' })
  .randomize({x: 30, y: 10});
  const arrowBG = arrow
  .grow({x: 10, y: 10})
  .randomize({x: 20, y: 10});
  shape.add(arrowBG);
  shape.add(arrow);
  shape.render()
  shape.wiggle(7)
  shape.pauseWiggle()
}
