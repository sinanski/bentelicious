import { dom } from '../../../render/ids';

const {
  container,
  overlay,
  choice_container,
  bg_flash,
  bg_gray,
  image
} = dom().answer;

export default function answerContainer(userImage) {
  return `
  <div id="${container}" class="answer-container" style="visibility: hidden">
    <div id="${overlay}" class="answer-overlay"></div>
    <div class="answer-bg">
      
      <svg id="${bg_gray}" viewBox="0 0 1310 1080" class="answer-flash" width="700" height="700" preserveAspectRatio="none">
        <polygon fill="#484c51" points="2500,0 2500,1080 20,1080 402,0 "></polygon>
      </svg>
        <img
          id="${image}"
          class="answer-user"
          src="${userImage}"
          alt="Tütenmann"
        />
      <svg id="${bg_flash}" viewBox="0 0 1310 1080" class="answer-flash" width="700" height="700" preserveAspectRatio="none">
        <polygon fill="black" points="416,0 396,0 300,0 280,0 207,241 263.8,204.7 90,780 112.5,775.1 0,1080 20,1080 68,1080 88,1080 254,573 228,540"></polygon>
        <polygon fill="white" points="286,0 217,226 274,189 97.7,775.3 120,771 6,1080 74,1080 240,573 214,540 402,0"></polygon>
      </svg>

    </div>
    <ul id="${choice_container}" class="answer-option">
    </ul>
  </div>
  `;
}
