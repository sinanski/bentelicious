import { dom } from '../../../render/ids';
const {
  dialog
} = dom();

export default function render(image, text, name) {
  return `
    <div id="${dialog.container}" class="dialog-container" style="visibility: hidden">
      <div class="dialog-image">
        <img
          id="${dialog.image}" 
          src="${image}" 
          alt="Bente dialog" 
         />
      </div>
      <div id="${dialog.bubble}" class="speech-bubble">
        <div id="dialog-name" class="dialog-name">
          <div class="dialog-name__inner">
            ${name.trim()}
          </div>
        </div>
        <p id="${dialog.text}">
          ${text}
        </p>
      </div>
    </div>
  `;
}

