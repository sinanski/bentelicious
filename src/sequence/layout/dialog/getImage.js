import bente_neutral from '../../../img/dialog/bente/neutral.png';
import bente_nice from '../../../img/dialog/bente/nice.png';
import bente_smirk from '../../../img/dialog/bente/smirk.png';
import bente_happy from '../../../img/dialog/bente/happy.png';
// import bente_moody from '../../../img/dialog/bente/neutral.png';
// import bente_chatbot from '../../../img/dialog/bente/neutral.png';
import user from '../../../img/dialog/user.PNG';
import user_female from '../../../img/dialog/user-female.PNG';
import user_male from '../../../img/dialog/user-male.PNG';
import cop from '../../../img/dialog/dsgvo-bulle.png';

const bente_moody = bente_smirk,
  bente_chatbot = bente_nice;

const images = {
  bente: bente_neutral,
  bente_nice,
  bente_moody,
  bente_chatbot,
  bente_smirk,
  bente_happy,
  user,
  user_female,
  user_male,
  cop
};

export function getDialogImage(name = '') {
  if(name.includes('.')) {
    return name
  } else {
    const current = images[name]
    return current
      ? current
      : images.bente
  }
}
