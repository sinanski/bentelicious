import {gsap} from "gsap";
import {ids} from "../../../render/ids";
import $ from "jquery";

export function dialogIn() {
  let tl = new gsap.timeline({defaults: {
    ease: "power4.out",
    duration: .6,
    immediateRender: true,
  }});
  tl
  .to(ids.dialog.container, {})
  .set(ids.dialog.container, {
    visibility: 'visible',
    pointerEvents: 'initial',
    y: 0,
  })
  .fromTo(ids.dialog.image, {
    duration: 1,
    y: '100%',
    rotate: 25
    },{
    y: 0,
    rotate: 0
  })
  .fromTo(ids.dialog.bubble, {
    y: '100%'
  }, {
    y: 0,
  }, '-=.4');
  return tl
}

export function dialogOut() {
  let tl = new gsap.timeline();
  // ids.dialog.container
  tl
  .set(ids.dialog.container, { pointerEvents: 'none' })
  .to(`${ids.dialog.image}, ${ids.dialog.bubble}`, {
    id: 'dialogOut',
    duration: .5,
    ease: "back.in(1.4)",
    stagger: .3,
    y: '100%',
  })
  .to(ids.dialog.container, {
    duration: .1,
    y: '100%'
  }, '+=0.8');
  return tl
}

export function answerIn() {
  // console.log(content)
  let subTl = new gsap.timeline({defaults: {
    ease: "power4.out",
    duration: .6
  }});
  subTl
  // .set(ids.answer.container, {clearProps:"all"})
  // .call(setCurrentAnswers, [content])
  .set(ids.answer.container, { visibility: 'visible', duration: 0 })
  .set(ids.answer.choices, { visibility: 'visible', duration: 0 })
  .fromTo(ids.answer.overlay, {
    x: '-100%',
  },{
    x: '0',
    })
  .fromTo(ids.answer.bg_flash, {
    y: '-100%',
  },{
    y: 0
  }, 0)
  .fromTo(ids.answer.bg_gray, {
    y: '100%',
  }, {
    y: 0
  }, 0)
  .fromTo(ids.answer.image, {
    ease: "back.out(1.7)",
    scale: 0,
  }, {
    scale: 1
  }, 0)
  .fromTo(ids.answer.choices, {
    duration: .3,
    ease: "back.out(1.7)",
    y: '-90vh',
    x: '10vw',
    // stagger: -.2,

  }, {
    y: 0,
    x: 0,
    stagger: -.2
  }, '-=.7');
  return subTl;
}

export function answerOut() {
  let subTl = new gsap.timeline();
  subTl
  .to(ids.answer.container, {x: '100%', duration: .2, opacity: 0})
  .set(ids.answer.container, {visibility: 'hidden'})
  return subTl
}

export function updateAnswer(id, content) {
  let tl = new gsap.timeline()
    tl
    .to(ids.dialog.bubble, {delay: 0, duration: 0.01})
    .to(ids.answer.text, {duration: 0, text:{ value: function(index) {
      if ( content[index] ) {
        return content[index].text
      }
      return 'Hide me!'
    }}})
    .to(ids.answer.choices, {visibility: function(index) {
      if ( !content[index] ) {
        return 'hidden'
      }
      return 'visible'
    }});
  return tl;
}


export function updateDialog(text, image) {
  let subTl = new gsap.timeline();
  // const image = single.image
  //   ? { attr: { src: single.image }}
  //   : {};
  subTl
  .to(ids.dialog.bubble, {delay: 0, duration: 0.01})
  .to(ids.dialog.text, {duration: 0, text:{ value:text }})
  .set(ids.dialog.image,{attr:{src:image}});
  // .set(ids.dialog.image, {image});
  return subTl;
}

function dialogImage() {
  const x = $(ids.dialog.image).attr('src');
  console.log(x)
}

function setCurrentAnswers(content) {
  const $elem = $(ids.answer.choices);
  $elem.each((i, elem) => {
    const child = $(elem);
    const p = child.find('.dialog-text');
    if ( content[i] ) {
      child.css('display', 'inherit');
      p.text(content[i].text)
    } else {
      child.css('display', 'none')
    }
  })
}
