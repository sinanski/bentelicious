import {find, get} from "lodash";
import * as dialog from './dialog';
import {getDialogImage} from '../getImage';
import SpeechBubble from '../../../../shapes/SpeechBubble';
import {ids} from "../../../render/ids";

export default class DialogAnimator {
  constructor({
    id,
    delay,
    onComplete,
    screens,
    image
              },
    renderer
              ) {
    this.id = id;
    this.delay = delay;
    this.screens = screens;
    this.renderer = renderer;
    this.image = getDialogImage(image);
    this.onComplete = onComplete
  }
  init() {
    dialog.dialogIn();
    return this;
  //   this.dialog = {
  //     inOut: dialog.dialogInOut(this.id, this.delay, this.onComplete),
  //     update: dialog.dialogUpdate(this.id, this.screens, this.image, this.renderer.getImage),
  //   };
  //   this.answer = {
  //     inOut: dialog.createAnswerInOut(),
  //     update: dialog.createUpdateAnswer(this.id, this.screens)
  //   };
  //   return this;
  }
  updateDialog(label, name) {
    console.log('update')
    if ( name ) {
      new SpeechBubble().setName('#dialog-name .dialog-name__inner', name)
    }
    dialog
      .dialogUpdate(this.id, this.screens, this.image, this.renderer.getImage)
      .play(label)
    // this.dialog.update.play(label)
  }
  answerIn(id) {
    dialog.createUpdateAnswer(this.id, this.screens);
    dialog.answerIn();
    // this.answer.update.play(id)
    // this.answer.inOut.play('in')
  }
  answerOut(id, content) {
    dialog.answerOut()
    // this.answer.inOut.play('out')
  }
  playIn() {
    dialog.dialogIn()
  }
  playOut() {
    dialog.dialogOut()
  }
  end() {
    this.dialog.inOut.play('out')
  }
  getCurrent(id) {
    const current = findCurrent(this.screens, id);
    const image = get(current.data, 'image') || this.image;
    const domId = ids.chat.avatar +  id;
    return {
      ...current,
      image,
      domId
    }
  }
}

function findCurrent(screens, id) {
  const current = find( screens, {id} )
  const currentLabel = find( screens, {label: id} )
  return current || currentLabel;
}
