import {gsap} from "gsap";
import * as animation from './dialog-animation';
import {getDialogImage} from '../getImage';


export function dialogInOut(id, delay, onComplete) {
  const tl = new gsap.timeline({ onComplete, delay });
  tl.addLabel(id)
  .add(animation.dialogIn(), 'in')
  .addPause()
  .add(animation.dialogOut(), 'out');
  return tl;
}

export function dialogIn() {
  return animation.dialogIn()
}

export function dialogOut() {
  return animation.dialogOut()
}

export function answerIn() {
  return animation.answerIn()
}

export function answerOut() {
  return animation.answerOut()
}

export function dialogUpdate(id, screens, img, getImage) {
  const tl = new gsap.timeline({ paused: true });
  screens.forEach(({
                     type,
                     text,
                     label,
                      data = {},
                     id
                   }) => {
    if ( type === 'TEXT' ) {
      const image = getImage(data.image || img);
      tl.add(
        animation.updateDialog(text, image),label || id
      )
      tl.addPause()
    }
  });
  return tl
}



export function createAnswerInOut() {
  const tl = new gsap.timeline({paused: true});
  tl.add(animation.answerIn(), 'in')
  .addPause()
  .add(animation.answerOut(), 'out')
  .addLabel('is out')
  return tl;
}

export function createUpdateAnswer(id, screens) {
  const tl = new gsap.timeline({ paused: true });
  screens.forEach(({
                     type,
                     content,
                     label,
                     id
                   }) => {
    if ( type === 'ANSWER' ) {
      tl.add(
        animation.updateAnswer(id, content),label || id
      )
      tl.addPause()
    }
  });
  return tl
}
