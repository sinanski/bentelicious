'use strict';
import $ from 'jquery';
import { orderBy, find } from 'lodash';
import Content from "./content";
import Scene from './Scene/Scene';
import RenderConfig from './render/RenderConfig';
import Renderer from './render/Renderer';

export default class Sequence {
  constructor() {
    onSceneComplete = onSceneComplete.bind(this);
    const user = window.user;
    const sceneList = new Content();
    this.user = user;
    this.dialogs = sceneList.dialogs;
    this.trigger = sceneList.trigger;
    this.state = {
      todo: todos(this.trigger),
      done: [],
    };
    const blockDialogs = $('body').hasClass('no-dialog');
    if ( !blockDialogs ) {
      this.init()
    } else {
      console.warn('No dialogs allowed on body of this page')
    }
  }
  init() {
    if ( this.dialogs.length ) {
      this.scene = createScene(this)
      this.attachEventListeners()
    }
  }
  setState(newState) {
    this.state = {
      ...this.state,
      ...newState
    }
  }
  addScene(scene) {
    const start = !this.state.todo.length
    this.dialogs = this.dialogs.concat(scene.dialogs);
    this.trigger = this.trigger.concat(scene.trigger);
    this.setState({
      todo: this.state.todo.concat(scene.trigger.id)
    });
    if ( start ) {
      this.setScene()
    }
  }
  setScene() {
    this.scene = createScene(this);
  }
  playIn() {
    if ( this.scene.timeline ) {
      this.scene.timeline.animator.playIn()
    }
    return this
  }
  playOut() {
    if ( this.scene.timeline ) {
      this.scene.timeline.animator.playOut()
    }
    return this
  }
  attachEventListeners() {
    const $menu = $('#toggle-menu');
    $menu.on('click', () => {
      const isOpen = $menu.hasClass('open-menu')
      if ( isOpen ) {
        this.playOut()
      } else {
        this.playIn()
      }
      // console.log(isOpen)
    })
    // console.log(this)
  }
}




function createScene(_this) {
  const current = getCurrent(_this);
  return new Scene(
    current,
    setupRenderer(_this),
    onSceneComplete
  )
}

function setupRenderer(_this) {
  return new Renderer(
    new RenderConfig(getCurrent(_this), onSceneComplete)
  );
}


function onSceneComplete() {
  this.setState({
      done: [
        ...this.state.done,
        this.state.todo[0]
      ],
      todo: [
        ...this.state.todo.slice(1, this.state.todo.length)
      ]
  });
  // this.scene = {};
  if ( this.state.todo.length ) {
    this.setScene()
  }
}

function getCurrent(_this) {
  const id = _this.state.todo[0];
  const dialog = find( _this.dialogs, {id} );
  const trigger = find( _this.trigger, {id} );
  return {
    ...dialog,
    ...trigger
  }
}

function todos(trigger) {
  const autoDialogs = trigger.filter(filterOnlyAutoTriggered);
  const sortedByPriority = orderBy(autoDialogs, ['priority'], ['desc']);
  return sortedByPriority.map(({ id }) => (
    id
  ));
}

function filterOnlyAutoTriggered(dialog) {
  return dialog.in === 'auto'
}
