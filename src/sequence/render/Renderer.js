import $ from 'jquery';
import {ids} from './ids';
import * as handler from './events/eventHandlers'

export default class Renderer {
  constructor({
    id,
    domIds,
    type,
    image,
    initialText,
    renderer,
    getImage,
    onSceneComplete
              }) {
    this.id = id;
    this.domIds = domIds;
    this.type = type;
    this.image = image;
    this.getImage = getImage;
    this.initialText = initialText;
    this.domIds = ids;
    this.onSceneComplete = onSceneComplete;
    this.renderDom = renderer;
  }
  init() {
    this.renderDom()
    return this;
  }
  render(id, elem) {
    $('#' + id).append(elem)
  }
  attachScene(_this) {
    this.scene = _this;
    return this;
  }
  attachEventHandlers({
    id,
    onDialogClick,
    onAnswer,
    onChatAnswer
                      }) {
    if ( onChatAnswer ) {
      handler.handleChatAnswerClick(id, onChatAnswer, this.scene)
    }
    //  EventListener für Shapes sollten innerhalb der Shapes verwaltet und hier
    //  höchstens Ein / Ausgeschaltet werden
  }
  onUnmount() {
    $('#scene').remove()
  }
}
