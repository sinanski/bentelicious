import $ from "jquery";
import {get} from "lodash";
import Shape from '../../shapes';
import PersonaStyle from '../../persona-style';
import SpeechBubble from '../../shapes/SpeechBubble';
import { getChatImage } from "../layout/chat/getImage";
import { getDialogImage } from '../layout/dialog/getImage';
import Answer from "../layout/dialog/dom/Answer";
import dialog from "../layout/dialog/dom/DOM_dialog";
import DOMChat from "../layout/chat/dom/DOM_chat";
import {ids, dom} from "./ids";

const scene = `
  <div id="${dom().scene}"></div>
`;

export default function RenderConfig(props, onSceneComplete) {
  const {
    id,
    image,
    screens,
    type,
  } = props;
  const first = get(screens, '[0]', {});
  this.type = type ? type.toUpperCase() : 'DIALOG';
  this.getImage = this.type === 'DIALOG'
    ? name => getDialogImage(name)
    : name => getChatImage(name);
  this.image = this.getImage(image || first.image);
  this.initialText = first.text;
  this.id = id;
  this.domIds = ids;
  this.screens = screens;
  this.onSceneComplete = onSceneComplete;
  this.renderer = this.type === 'DIALOG'
    ? () => renderDialog(this.image, this.initialText)
    : () => renderChat(this.image, this.initialText, this.screens)
}



function renderDialog(image, text, name = 'Bente') {
  $('body').append(scene);
  const $scene = $(ids.scene);
  const answer = new Answer();
  const bubble = new SpeechBubble({className: 'speech-bubble-dialog'});
  $scene
    .append(dialog(image, text, name))
    .append(answer.container);
  bubble.render(ids.dialog.bubble);
  $(ids.answer.choice_container).html(answer.choice);
  answer.renderArrow()
}

function renderChat(image, text) {
  $('body').append(scene);
  const $scene = $(ids.scene);
  const chat = DOMChat(image, text);
  $scene.append(chat)
}
