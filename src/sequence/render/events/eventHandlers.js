import $ from "jquery";
import {ids} from "../ids";
export const buttonSpeechBubble = 'speech-bubble-button';

export function handleContainerClick(onClick, _this) {
  $(ids.dialog.container).on("click", function(e) {
    const isAnswer = $(e.target).hasClass('dialog-choice')
    if ( !isAnswer ) {
      onClick(_this);
    }
  });
}

export function handleAnswerClick(onClick, _this) {
  $(ids.answer.choices).on("click", function (e) {
    const $target = $(e.target);
    const index = $target.parent().index();
    onClick(index, _this);
  });
}

export function handleChatAnswerClick(id, onClick, _this) {
  const thisId = `#answer-options${id} .dialog-choice`;
  $(thisId).on("click", function (e) {
    const $target = $(e.target);
    const index = $target.index();
    _this.onAnswerFromTimeline(index, _this)
    onClick(index, _this);
  });
}

export function handleAnswerHover(onHover) {
  $(ids.answer.choices).on({
    mouseenter: function (e) {
      handleHover(e, onHover).wiggle(7)
    },
    mouseleave: function (e) {
      handleHover(e, onHover).pauseWiggle(7)
    }
  });
}

function handleHover(e) {
  const $elem = $(e.target);
  const id = $elem.find('svg').attr('id');
  // onHover(id)
  return window.shapeController[id]
}

export function handleProgress(onClick) {
  $(ids.dialog.container).on("click", function (e) {
    onClick();
  });
}
export function handleDialogButton(onClick) {
  $('.' + buttonSpeechBubble).on("click", function (e) {
    onClick($(e.target));
  });
}

export function handleDialogBreak(id, onClick) {
  $('#' + id).on('click', function () {
    onClick()
  })
}
