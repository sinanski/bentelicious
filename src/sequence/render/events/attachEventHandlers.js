import * as handler from './eventHandlers';

export default function attachEventHandlers({
  onDialogClick,
  onAnswer,
  onAnswerHover,
  onDialogButtonClick,
  _this,
}) {
  // console.log('attach event handlers')
  handler.handleContainerClick(onDialogClick, _this);
  handler.handleAnswerClick(onAnswer, _this);
  handler.handleAnswerHover(onAnswerHover);
  handler.handleDialogButton(onDialogButtonClick);
}
