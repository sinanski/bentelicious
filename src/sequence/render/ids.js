export const ids = {
  scene: '#scene',
  source: '#dialog-screen',
  dialog: {
    container: '#dialog-container',
    image: '#dialog-image',
    bubble: '#dialog-bubble',
    text: '#dialog-text'
  },
  answer: {
    container: '#answer',
    overlay: '#answer-overlay',
    choice_container: '#answer-options',
    choices: '#answer-options .dialog-choice',
    text: '#answer-options .dialog-choice p',
    bg_flash: '#answer-bg-flash',
    bg_gray: '#answer-bg-gray',
    image: '#answer-user'
  },
  chat: {
    content: '#chat-content',
    avatar: '#avatar'
  }
};


export function dom() {
  let newIds = rawObj(ids)
  for ( let i in ids ) {
    for ( let j in ids[i] ) {
      const isObject = typeof ids[i] === 'object';
      const isId = ids[i][j].startsWith('#')
        && !ids[i][j].includes(' ');
      if ( isObject && isId ) {
        newIds[i][j] = ids[i][j].replace('#', '')
      }
    }
  }
  return newIds
}

function rawObj(ids) {
  let newIds = {}
  for ( let i in ids ) {
    newIds[i] = typeof ids[i] === 'object' ? {} : ids[i].replace('#', '')
  }
  return newIds
}
