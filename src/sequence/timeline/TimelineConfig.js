import ChatAnimator from '../layout/chat/animation/ChatAnimator';
import DialogAnimator from '../layout/dialog/animation/DialogAnimator';

export default function TimelineConfig(props) {
  const {
    id,
    type = 'DIALOG',
    delay,
    screens,
    image,
    renderer,
    onComplete,
    ...data
  } = props;
  this.id = id;
  this.delay = delay;
  this.image = image || screens[0].image;
  this.text = screens[0].text;
  this.screens = screens;
  this.onComplete = onComplete;
  this.data = data;
  this.type = type;
  this.renderer = renderer;
  this.animator = type === 'DIALOG'
    ? new DialogAnimator(this, renderer)
    : new ChatAnimator(this, renderer)
}
