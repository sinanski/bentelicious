export default class Timeline {
  constructor({
                onComplete,
                animator,
              }) {
    this.animator = animator;
    this.flash = 'flash timeline';
    this.onComplete = onComplete;
    animator.init()
  }
  updateDialog(label, name) {
    this.animator.updateDialog(label, name)
  }
  answerIn(id) {
    this.animator.answerIn(id)
  }
  answerOut(id, content) {
    this.animator.answerOut(id, content)
  }
  flashIn() {
  //  Diese Methode bleibt hier, da sie in beiden Fällen gleich ist
  }
  flashOut() {

  }
  end() {
    // const mustLeave = this.answer.inOut.currentLabel() !== 'is out';
    // if ( mustLeave ) {
    //   this.answerOut()
    // }
    this.animator.end()
  }
}
