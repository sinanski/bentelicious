export async function subScene(current = {}, clicked = {}) {
  const subScene = clicked.subScene || current.subScene || _nullCallback;
  return await subScene()
}

async function _nullCallback() {
  return await timeout(0);
}

function timeout(ms) {
  return new Promise(resolve => setTimeout(resolve, ms));
}
