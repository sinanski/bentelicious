import fallbackImage from '../../img/dialog/bente-nice.PNG';
import ID from '../../utils/idGenerator';

window.testScene = new SceneFromText({textList: ['1', '2']})

export default function SceneFromText({
                                        id = ID(),
  priority = 2,
  playIn = 'auto',
  delay = 0,
  onComplete,
  label,
  name = 'Bente',
                                        type = 'CHAT',
                                        textList = ['missing'],
                                        image = fallbackImage
}) {
  this.dialogs = {
    id,
    image,
    type,
    label,
    name,
    // data: {},
    screens: createScreens(textList)
  };
  this.trigger = {
    id,
    priority,
    delay,
    in: playIn,
    onComplete
  };
  // this.screens = createScreens(textList);
}

function createScreens(textList) {
  const list = typeof  textList === 'string' ? [ textList ] : textList;
  return list.map(text => ({
    id: ID(),
    text,
    data: {},
    type: "TEXT",
  }));
}
