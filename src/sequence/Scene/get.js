import {find, findIndex} from "lodash";

export function screen(target, _this) {
  return target
    ? find(_this.screens, { label: target })
    : next(_this);
}

export function next(_this) {
  const id = _this.state.thisId;
  const i = findIndex( _this.screens, {id} ) + 1;
  return _this.screens[i]
}