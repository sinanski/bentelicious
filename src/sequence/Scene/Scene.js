import { find } from 'lodash';
import * as on from './sceneHandler';
import Timeline from '../timeline/Timeline';
import Flash from '../flash';
import TimelineConfig from '../timeline/TimelineConfig';
import attachEventHandlers from '../render/events/attachEventHandlers';

export default class Scene {
  constructor({
      id,
      delay,
      image,
      screens,
      out,
      initial,
      type
    },
      renderer,
      onComplete = () => {}
              ) {
    onCompleteScene = onCompleteScene.bind(this);
    this.onAnswerFromTimeline = this.onAnswerFromTimeline.bind(this);
    this.id = id;
    this.delay = delay;
    this.image = image;
    this.onSequenceComplete = onComplete;
    this.screens = screens;
    this.renderer = renderer.init().attachScene(this);
    this.flash = new Flash().render();
    const config = new TimelineConfig({
      id,
      type,
      delay,
      image,
      screens,
      renderer,
      onComplete: onCompleteScene,
      onAnswer: this.onAnswerFromTimeline
    });
    this.timeline = new Timeline(config);
    // Die Timeline muss eigene Eventhandler bekommen
    attachEventHandlers({
      onDialogClick: on.dialogClick,
      onAnswer: on.answer,
      _this: this,
    });

    this.state = {
      thisId: screens[0].id,
      pauseDialog: false,
    };
  }

  setState(newState) {
    this.state = {
      ...this.state,
      ...newState
    }
  }
  current() {
    const id = this.state.thisId;
    return find( this.screens, {id} )
  }
  // play() {
  //   const next = getNext(this);
  //   progress(next, this);
  //   return this;
  // }
  onAnswerFromTimeline(index, _this) {
    // _this.setState({ pauseDialog: true });
    on.answer(index, _this)
  }
}

function onCompleteScene() {
  this.renderer.onUnmount();
  this.onSequenceComplete()
}
