import * as get from "./get";
import {subScene} from "./subScene";

export function dialogClick(_this) {
  const { data = {} } = _this.current();
  const screen = get.screen(data.target, _this);
  subScene(data)
  .then(() => {
    progress(screen, _this)
  })
}

export function answer(i, _this) {
  const { content, id, data } = _this.current();
  const clicked = content[i].data || {};
  subScene(data, clicked)
  .then(() => {
    const screen = get.screen(clicked.target, _this);
    _this.setState({ pauseDialog: false });
    progress(screen, _this)
    _this.timeline.answerOut(id, content[i])
  })

}

export function progress(screen, _this) {
  if ( !_this.state.pauseDialog ) {
    if (!screen) {
      _this.timeline.end()
    } else {
      _this.setState({thisId: screen.id});
      typeSwitch(screen, _this)
    }
  }
}

function typeSwitch(screen, _this) {
  const {
    type,
    content,
    id,
    label,
    data,
    text
  } = screen;
  switch(type) {
    case 'ANSWER' :
      _this.setState({ pauseDialog: true });
      _this.timeline.answerIn(label || id, content);
      break;
    case 'FLASH' :
      _this.flash.playIn(text);
      break;
    default:
      _this.timeline.updateDialog(
        label || id,
        data.name
      )
  }
}
