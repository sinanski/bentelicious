import $ from 'jquery';
import { onBlur } from './onBlur';
import { submit } from './submit';
import scooterImage from '../img/contact/animated-bente-roller.gif';

const Scooter = () => (`
  <img
    id="scooter-img"
    src=${scooterImage}
    class="scooter"
    alt="bente post"
  >
      
`)


if ($('#contact').length) {
  onBlur();
  submit();
  $('#bente-roller').append(Scooter())
}


