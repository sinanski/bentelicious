import * as animation from "./animation";
import SceneFromText from '../sequence/Scene/SceneFromText';
import {getDialogImage} from '../sequence/layout/dialog/getImage';

export default class SubmitHandler {
  constructor() {
    this.i = 0;
    this.image = undefined;
    this.success = animation.onSuccess();
    this.error = animation.onError();
    this.validationProblems = [];
    this.timeline = {};
    // renderScooter()
  }
  onSuccess() {
    this.success.play()
  }
  onError() {
    this.error.play()
  }
  addValidationProblem($elem) {
    let text = '';
    switch(this.i) {
      case 2 :
        this.image = getDialogImage('bente_moody');
        text = 'Kapierst du es wirklich nicht, oder willst du mich verarschen?';
        break;
      case 3 :
        text = 'Wenn du mir nichts zu sagen hast, bist du hier falsch.';
        break;
      default :
        text = $elem.data('validate');
    }
    this.validationProblems = this.validationProblems.concat(text);
  }
  renderValidationProblem() {
    window.sequence.addScene(
      new SceneFromText({
        textList: this.validationProblems,
        type: 'DIALOG',
        image: this.image
      })
    );
    this.i ++;
    this.resolveValidationsProblems()
  }
  resolveValidationsProblems() {
    this.validationProblems = [];
  }
}

// function renderScooter() {
//   $('#on-send').append(Scooter())
// }
//
// const Scooter = () => (`
//   <div id="stage" class="dialog-container">
//       <div id="bente-roller">
//         <img
//           id="scooter-img"
//           src=${scooterImage}
//           class="scooter"
//           alt="bente post"
//         >
//       </div>
//     </div>
// `)
