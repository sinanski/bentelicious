"use strict";
import $ from 'jquery';

export function onBlur() {
  $('.input').each( () => {
    $(this).on('blur', () => {
      if( $(this).val().trim() !== "" ) {
        $(this).addClass('has-val');
      }
      else {
        $(this).removeClass('has-val');
      }
    })
  });
}
