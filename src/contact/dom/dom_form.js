export const form = `
  <div class="container">
    <div class="row justify-content-center">
      <div class="col-sm-10">

        <div class="contact">
          <div class="contact__form shadow-wide">
            <div class="contact__form__body">
              <h4>Kontaktieren Sie uns</h4>


              <form id="contact-form" class="contact2-form validate-form">

                <div class="wrap-input2 validate-input" data-validate="Name is required">
                  <input class="input2" type="text" name="name">
                  <span class="focus-input2" data-placeholder="NAME"></span>
                </div>

                <div class="wrap-input2 validate-input" data-validate="Etwas stimmt nicht mit der E-Mail Adresse: ex@abc.xyz">
                  <input class="input2" type="text" name="email">
                  <span class="focus-input2" data-placeholder="EMAIL"></span>
                  <div class="invalid-feedback">
                    Wem können wir antworten
                  </div>
                </div>

                <div class="wrap-input2 validate-input" data-validate="Bitte geben Sie eine Nachricht ein">
                  <textarea class="input2" name="message"></textarea>
                  <span class="focus-input2" data-placeholder="MESSAGE"></span>
                  <div class="invalid-feedback">
                    Wie können wir helfen
                  </div>
                </div>

                <div class="container-contact2-form-btn">
                  <input
                    type="submit"
                    value="Los geht's"
                    class="btn btn-primary"
                    onclick="submitForm('contact-form')"
                  >
                </div>
              </form>

              <div class="form-success hidden">
                Danke für Ihre Nachricht.<br>
                Wir werden uns so schnell wie möglich bei Ihnen melden.
              </div>

              <div class="form-error hidden">
                <p>
                  Etwas ist schief gegangen.
                  <br>
                  Leider ist beim übermitteln der Nachricht ein Fehler aufgetreten. Bitte überprüfen Sie Ihre Internetverbindung und versuchen es noch einmal.
                  <br><br>
                  Oder schreiben Sie uns direkt an <a href="mailto:info@baggid.com" class="link" target="_blank">info@baggid.com</a>
                </p>
              </div>

            </div>
          </div>
          <div class="contact__card">
            <h6 class="contact__head">BAGGID GmbH</h6>
            <div class="contact__card__single-item">
              <div class="col-xs-2">
                <i class="glyphicon glyphicon-map-marker"></i>
              </div>
              <div class="col-xs-10">
                <p>Treuchtlinger Str. 4,<br>10779 Berlin</p>
              </div>
            </div>
            <div class="contact__card__single-item">
              <div class="col-xs-2">
                <i class="glyphicon glyphicon-envelope"></i>
              </div>
              <div class="col-xs-10">
                <a href="mailto:info@baggid.com" target="_blank">info@baggid.com</a>
              </div>
            </div>
            <div class="contact__card__single-item">
                <div class="col-xs-2">
                  <i class="glyphicon glyphicon-phone"></i>
                </div>
                <div class="col-xs-10">
                  <p>+49 (0)  30 - 609 29 003</p>
                </div>
              </div>
          </div>
        </div>
      </div>
    </div>
  </div>
`;
