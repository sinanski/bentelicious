'use strict';
import $ from 'jquery';
import Email from './Email';
import SubmitHandler from './SubmitHandler';

export function submit() {
  const handler = new SubmitHandler();

  window.submitForm = function (id) {
    event.preventDefault();
    const email = $(`#${id} .validate-input input[name="email"]`);
    const message = $(`#${id} .validate-input textarea[name="message"]`);
    let check = true;
    const isEmailInvalid = $(email)
      .val()
      .trim()
      .match(/^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{1,5}|[0-9]{1,3})(\]?)$/) == null;
    const isMessageInvalid = $(message).val().trim() == '';
    if ( isEmailInvalid ) {
      showValidate(email, handler);
      check = false;
    }
    if ( isMessageInvalid ) {
      showValidate(message, handler);
      check = false;
    }
    if (check) {
      submitMail(id, handler)
    } else {
      handler.renderValidationProblem()
    }
  }
}

async function submitMail(id, handler) {
  event.preventDefault();
  const data = getData(id);
  const response = await postForm(data);
  if (response === 'OK') {
    onSuccess(handler)
  } else {
    onError(handler)
  }
}

function showValidate(input, handler) {
  const thisAlert = $(input).parent();
  handler.addValidationProblem(thisAlert)
}

function onSuccess(handler) {
  handler.onSuccess()
}

function onError(handler) {
  handler.onError();
}

function renderFeedback(id, className) {
  $('#' + id).siblings( `.${className}` ).removeClass('hidden');
}

function hideContactForm(id) {
  $('#' + id).addClass('hidden')
}

function getData(id) {
  const newId = id ? '#' + id : '#contact-form';
  return $(newId).serializeArray();
}

function getMessage(data) {
  let str = '';
  data.forEach(function(message) {
    str += `${message.name}: ${message.value} - `
  });
  return str;
}

// async function postForm() {
//   return 'OK';
// }

async function postForm(data) {
  const body = getMessage(data);
  return await Email.send({
    SecureToken : 'f8a43247-bda7-400f-b325-052005017696',
    To : 'bente.theuvsen@gmail.com',
    From : "sinan.alk@gmail.com",
    Subject : "Von deiner Website",
    Body : body
  });
}
