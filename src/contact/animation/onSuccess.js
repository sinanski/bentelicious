import {gsap} from "gsap";
import CustomEase from "../../gsap/CustomEase";
gsap.registerPlugin(CustomEase)

export function scooter() {
  const tl = new gsap.timeline();
  tl
  .set('#on-send', {
    display: 'block'
  })
  .fromTo('#bente-roller', {
    x: -400
  }, {
    duration: 2,
    ease: "none",
    x: 1400
  })
  return tl;
}

export function sendLetter() {
  const tl = new gsap.timeline()
  .fromTo('#calling-card', {
    x: 0,
    scale: 1,
  }, {
    delay: .7,
    duration: 1.2,
    x: 1400,
    scaleY: .8,
    scaleX: 1.2,
    ease: CustomEase.create("custom", "M0,0,C0.272,0,0.393,-0.098,0.61,-0.098,0.938,-0.098,0.996,0.6,1,1")
  })
  return tl
}