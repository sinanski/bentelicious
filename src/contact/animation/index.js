import {gsap} from 'gsap';
import { scooter, sendLetter } from './onSuccess';
import { onErrorAnimation } from './onError';

export function onSuccess() {
  const tl = new gsap.timeline({paused: true})
  .add(scooter(), 0)
  .add(sendLetter(), 0)
  return tl;
}

export function onError() {
  return onErrorAnimation()
}
