import {gsap} from "gsap";
import errorBackground from "../../img/contact/contact-error.png";

export function onErrorAnimation() {
  const tl = new gsap.timeline({paused: true});
  tl.fromTo('#calling-card', {
    rotateY: 0
  }, {
    duration: .25,
    ease: "power2.in",
    rotateY: 90
  })
  .set('#calling-card', {
    css:{
      backgroundImage: `url(${errorBackground})`
    }
  })
  .set('.calling-card__content', {
    display: 'none'
  })
  .set('.error-content', {
    display: 'flex'
  })
  .to('#calling-card', {
    duration: .25,
    ease: "power2.out",
    rotateY: 180
  })
  return tl;
}