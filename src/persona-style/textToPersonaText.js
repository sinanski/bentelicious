import Shape from '../shapes';

export function createSpeechBubble() {
  const shape = new Shape();
  const speechBubble = shape.speechBubble({
    fill: 'black',
    fillOpacity: .85,
    stroke: 'black',
    strokeWidth: 12,
  });
  const white = speechBubble.grow({
    x: -30,
    y: -30,
    fill: 'none',
    stroke: 'white',
    strokeWidth: 10,
  });
  shape.add(speechBubble);
  return shape
}

export function createBackground($font) {
  const width = $font.width();
  const height = $font.height() / 2.5;
  const fill = $font.hasClass('invert') ? 'white' : 'black';
  const shape = new Shape({width, height});
  const rectangle = shape
    .rectangle({
      full: true,
      fill
    })
    .randomize({x: 40, y: 10})
    .displace({y: 10});
  shape.add(rectangle);
  return shape.createDomObject();
}

export function textToPersonaText($elem) {
  const fill = !$elem.hasClass('invert') ? 'white' : 'black';
  const word = $elem.html();
  return invert(word, fill);
}

export function invert(word, fill = 'black', width, height, className) {
  const length = word.length;
  const randomIndex = getRandomLetter(length);
  const randomRotation = getRandomVal(length)
  let string = '';
  for( let i = 0; i < length; i++ ) {
    const letter = word[i];
    if ( i === randomIndex ) {
      string += createInvert(letter, fill, width, height, className)
    } else {
      const rotate = i === randomRotation;
      string += createRegular(letter, rotate)
    }
  }
  return string;
}


function createRegular(letter, rotate) {
  switch(true) {
    case(rotate) :
      return `
        <span class="regular rotate">
          ${letter.trim()}
        </span>
      `;
    case( letter === ' '):
      return '<span style="width: 20px"></span>'
    default :
      return letter.trim()
  }
}

function createInvert(letter, fill, width = 40, height = 60, className) {
  const shape = new Shape({className});
  const rectangle = shape
  .rectangle({
    width,
    height,
    fill
  })
  .randomize({x: 10, y: 20})
  shape.add(rectangle);
  const background = shape.createDomObject();
  return `<span class="invert">${letter}${background}</span>`;
}

function getRandomVal(length) {
  return  Math.floor(Math.random() * length);
}

function getRandomLetter(length) {
  return Math.floor(Math.random() * ( length - 2 )) + 1;
}
