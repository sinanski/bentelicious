// <div class="persona-font">Persona</div>
// <div class="persona-font invert">Persona</div>
// <div class="persona-font randomize">Persona</div>
// <div class="persona-background">Persona bg</div>
// <div class="persona-background invert">Persona bg</div>
// <div class="persona-background double">Persona bg</div>
// <div class="persona-background double invert">Persona bg</div>
import $ from 'jquery';
import {
  textToPersonaText,
  invert,
  createBackground,
  createSpeechBubble
} from './textToPersonaText';
import Shape from "../shapes";
import SpeechBubble from '../shapes/SpeechBubble';

export default class PersonaStyle {
  constructor() {
    // this.renderClasses()
  }
  renderClasses() {
    handleFonts();
    handleShapes();
    handleBackgrounds();
    handleForm();
    handleButton();
  }
  invertLetter(word = '', fill, width, height, className) {
    return invert(word.trim(), fill, width, height, className)
  }
};

function handleButton() {
  const $button = $('.persona-button');
  if ( $button.length ) {
    $button.each(( i, bg ) => {
      const shape = new Shape({ width: 100, height: 100, className:"shape-parent adept" });
      const rect = shape.rectangle({fill: 'black'}).randomize({x: 15, y: 10});
      const background = rect.grow({x: 1, y: 1, fill: 'white'}).randomize({x: 2, y: 20});
      shape.add(background)
      shape.add(rect)
      const domNode = shape.createDomObject()
      $(bg).append(domNode)
    })
  }
}

function handleForm() {
  handleInput();
  handleTextArea();
}

function handleTextArea() {
  const $background = $('.persona-textarea');
  if ( $background.length ) {
    $background.each(( i, bg ) => {
      const shape = new Shape({ width: 100, height: 100, className:"shape-parent adept" });
      const rect = shape
        .rectangle({})
        .randomize({x: 4, y: 15});
      const background = rect
        .grow({x: 0, y: 3, fill: 'black'})
        .randomize({x: 3, y: 1});
      shape.add(background)
      shape.add(rect)
      const domNode = shape.createDomObject()
      $(bg).append(domNode)
    })
  }
}

function handleInput() {
  const $background = $('.persona-input');
  if ( $background.length ) {
    $background.each(( i, bg ) => {
      const shape = new Shape({ width: 100, height: 100, className:"shape-parent adept" });
      const rect = shape
        .rectangle({})
        .randomize({x: 4, y: 10});
      const background = rect
        .displace({y: -12, x: 0, fill: 'black'})
        .randomize({x: 3, y: 1});
      shape.add(background)
      shape.add(rect)
      const domNode = shape.createDomObject()
      $(bg).append(domNode)
    })
  }
}



function handleBackgrounds() {
  const $background = $('.persona-background');
  if ( $background.length ) {
    console.log('not yet handled')
  }
}

function handleShapes() {
  const $speechBubble = $('.speech-bubble');
  if ( $speechBubble.length ) {

    const shape = new SpeechBubble()
    $speechBubble.append(shape.createDomObject())
    shape.wiggle()
  }
}

function handleFonts() {
  const $fonts = $('.persona-font');
  $fonts.each(( i, font ) => {
    const $font = $(font);
    const span = textToPersonaText($font)
    const background = createBackground($font);
    $font
      .empty()
      .append(span)
      .append(background)
  })

}
