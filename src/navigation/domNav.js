const subPages = {
  home: 'Home',
  portfolio: 'Portfolio',
  'https://www.etsy.com/de/shop/Bentelicious': 'Shop',
  about: 'Über mich',
  contact: 'Kontakt'
};

export function createDomNav(location) {
  const items = navItems(location);
  return items + domNav;
}

function navItems(location) {
  return `
  <div id="nav-items" class="items">
    ${createNavItems(location)}
  </div>
  `
}

function createNavItems(location) {
  let links = '';
  for (let i in subPages) {
    const link = getLink(location, i)
    links += domLink(link, subPages[i])
  }
  return links;
}

function domLink(link, text) {
  const target = link.includes('http')
    ? 'target="_blank" rel="noopener"'
    : '';
  return `<a ${target} href="${link}">${text}</a>`
}

function getLink(current, link) {
  if ( current === link ) {
    return '#'
  }
  return createLink(current, link)
}

function createLink(current, link) {
  if ( link.includes('http') ) {
    return link;
  }
  let prefix = current !== 'home' ? '../' : '';
  const target = link !== 'home' ? link : '';
  return prefix + target
}

const domNav = `
<button id="toggle-menu" class="btn close-icon "></button>
<div id="menu-active-bg" class="menu-active-bg hidden"></div>
<div id="nav-bg" class="nav__bg">
  <div class="nav__bg-black">
    <div class="nav-content">
    </div>
  </div>
</div>
<div id="nav-overlay" class="nav__page-overlay"></div>
`;
