import { gsap } from "gsap";
import $ from "jquery";

const defaultDisplacement = {
  x: "-20%",
  y: "-40%",
  scale: 4,
};

// Bitte auslagern und schöner machen
let contentDisplace = $('#content').attr("data-position");
contentDisplace = contentDisplace
  ? JSON.parse(contentDisplace)
    ? JSON.parse(contentDisplace)
    : defaultDisplacement
  : defaultDisplacement;

const {
  x = '-20%',
  y = '-40%',
  scale = 4
} = contentDisplace;

export default function animation() {

  let tl = gsap.timeline({paused: true});

  tl.from("#nav-bg", {
    duration: .75,
    ease: "power2.out",
    x: '120vw'
  })

  .from("#nav-items a", {
    duration: 1,
    ease: "back.out(3)",
    stagger: 0.05,
    x: '120vw'
  },'-=.75')

  .from("#nav-overlay", {
    duration: .5,
    ease: "power1.out",
    x: '120vw'
  },'-=1')

  .from("#menu-active-bg", {
    duration: .5,
    ease: "power1.out",
    x: '120vw'
  },'-=1.5')

  .to("#content", {
    duration: .8,
    ease: "power2.inOut",
    x,
    y,
    scale
  },'-=1');
  return tl;
}