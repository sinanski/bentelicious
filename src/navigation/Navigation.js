import $ from "jquery";
import {createDomNav} from './domNav';
import animation from "./animation";
import getStyle from "./style";

const nav = $('#nav');

export default class Navigation {

  constructor() {
    this.location = nav.data('location') || 'home';
    const domNav = createDomNav(this.location);
    nav.removeClass('hidden');
    nav.html(domNav);
    this.menuButton = $('#toggle-menu');
    this.id = null;
    this.width = 0;
    this.index = -1;
    this.isOpen = false;
    this.wiggle = $('#menu-active-bg');
    this.menuItem = $('#nav-items a');
    this.timeline = animation();
    this.menuItem.on('mouseover', e => this.onMenuActivate(e));
    this.menuButton.on('click', () => this.toggle())
  }

  onMenuActivate(e) {
    const index = $(e.target).index();
    this.wiggle.removeClass('hidden');
    this.width = e.target.offsetWidth;
    this.index = index;
    const isActive = this.id === e.target.textContent;
    if (!isActive) {
      this.activateMenuItem(e);
    }
  };

  activateMenuItem(e) {
    this.id = e.target.textContent;
    const style = getStyle(e.target);
    this.wiggle.css(style);
    this.menuItem.removeClass('active');
    $(e.target).addClass("active");
  };

  toggle() {
    if ( !this.isOpen ) {
      this.openMenu()
    } else {
      this.closeMenu()
    }
  };

  openMenu() {
    this.timeline.play();
    this.menuButton.removeClass('close-menu');
    this.menuButton.addClass('open-menu');
    this.isOpen = true;
    window.ui.setState({ menuOpen: true })
  };

  closeMenu() {
    this.isOpen = false;
    this.menuButton.removeClass('open-menu');
    this.menuButton.addClass('close-menu');
    this.timeline.reverse();
    window.ui.setState({ menuOpen: false })
  };
}
