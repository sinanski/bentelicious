export default function getStyle({ offsetTop, offsetHeight, offsetLeft }) {
  const top = getTop(offsetTop, offsetHeight);
  const left = getLeft(offsetLeft);
  const transform = getTransform(offsetHeight);
  return { top, left, transform };
};

function getTop(offsetTop, offsetHeight) {
  return offsetTop + (offsetHeight / 2) - 65;
}

function getLeft(offsetLeft) {
  return offsetLeft - 60;
}

function getTransform(offsetHeight) {
  const scale = getScale(offsetHeight);
  return `scaleY(${scale})`;
}

function getScale(offsetHeight) {
  return 1 + ( offsetHeight / 35 - 1 ) / 2;
}

