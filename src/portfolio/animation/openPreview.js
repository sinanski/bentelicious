import $ from "jquery";
import {gsap} from "gsap";


export function openPreview(id, onComplete) {
  let tl = new gsap.timeline({paused: true, onComplete});
  tl
  .add(animateAppOpen(id), 0)
  .add(animateImagePreviewIn(id), 0);
  return tl;
}

function animateImagePreviewIn(id) {
  const i = $('#' + id).index()
  const originX = 10 + 25 * i;
  const transformOrigin = `${originX}% 10%`;
  const tl = gsap.timeline();
  tl
  .set('#image-preview', {
    visibility: 'visible',
    transformOrigin
  })
  .fromTo('#image-preview', {
    duration: .3,
    ease: "power2.out",
    opacity: 0,
    scale: .5,
  }, {
    opacity: 1,
    scale: 1,
  }, 0)
  .to('#portfolio-background', {
    scale: 1.2,
    duration: .3,
    ease: "power2.out",
  }, 0)
  return tl;
}

function animateAppOpen(id) {
  const distance = distanceToCenter(id);
  const tl = gsap.timeline();
  tl
  .to('#' + id, {
    duration: .3,
    scale: 4,
    x: distance.x,
    y: distance.y,
    opacity: 0
  }, 0)
  .to('.app', {
    duration: .2,
    opacity: 0
  }, 0)
  // .set('#' + id, {display: 'none'})
  return tl
}

function distanceToCenter(id) {
  const $frame = $('#portfolio-frame');
  const centerX = $frame.width() / 2;
  const centerY = $frame.height() / 2;
  const offset = elementOffset(id);
  return {
    x: centerX - offset.centerX,
    y: centerY - offset.centerY,
  }
}

function elementOffset(id) {
  const $elem = $('#' + id);
  const offset = $elem.offset();
  return {
    centerX: offset.left,
    centerY: offset.top
  }
}
