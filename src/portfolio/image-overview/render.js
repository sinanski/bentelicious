import $ from 'jquery';
import dom_image_overview from './dom_image_overview';

export function render(images) {
  $('#image-preview').remove()
  $('#portfolio-frame').append(dom_image_overview(images))
}
