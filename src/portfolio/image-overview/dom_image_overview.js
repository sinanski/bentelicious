export default function dom_image_overview(images) {
  const dom_images = image(images)
  return `
    <div id="image-preview" class="image-preview overlay" style="visibility: hidden">
      ${dom_images}
    </div>
  `
}

function image(images) {
  let dom_object = '';
  for ( let i in images ) {
    const src = images[i].default;
    dom_object += dom_image_test( i, src )
  }
  return dom_object;
}

const dom_image = ( i, src ) => (`
  <div class="image-preview__img">
    <img id="${i}" src="${src}" alt="${i}" />
  </div>
`)

const dom_image_test = ( i, src ) => (`
  <div class="image-preview__img">
    <div class="dom-image"></div>
  </div>
`)
