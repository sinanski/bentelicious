import $ from 'jquery';
import dom_ipad from './dom_ipad';
import { dom_app } from './dom_app';
import { eventHandler } from './eventHandlers';

const apps = {
  fledermaeuse: 'Nacktaktive Fledermäuse',
  character: 'Character Designs',
  unplugged: 'Unplugged',
  'live-graphic-novel': 'Live-Graphic-Novel'
};

// function importAll(r) {
//   console.log(r)
//   return r.keys().map(r);
// }
function importAll(r) {
  let images = {};
  r.keys().map((item, index) => { return images[item.replace('./', '')] = r(item); });
  return images;
}

const images = importAll(require.context('../../img/portfolio/ipad/apps', false, /\.(png|jpe?g|svg|gif)$/));

// const images = importAll(require.context('../img/portfolio/fledermaeuse', false, rule));

export function render() {
  const apps = renderApps()
  $('#portfolio').html(dom_ipad(apps))
  eventHandler(images)
}

function renderApps() {
  let domObject = '';
  for ( let id in apps ) {
    const imageKey = `app-${id}.png`;
    const image = images[imageKey].default
    domObject += dom_app(id, apps[id], image)
  }
  return domObject
}
