import iPad from "../../img/portfolio/iPad.png"

export default function dom_ipad(apps) {
  return `
    <div id="portfolio-container" class="portfolio-container">
      <img
        id="ipad-image"
        src=${iPad}
        class="iPad"
        alt="iPad"
      >
      <div class="portfolio">
        <buttton id="home-button" class="btn home-button"></buttton>
        <div id="portfolio-frame" class="portfolio-frame">
          <div id="portfolio-background" class="portfolio__background"></div>
          <div class="portfolio__apps">
            ${apps}
          </div>
        </div>
      </div>
    </div>
`;
}
