export function dom_app(id, name, image) {
  return `
    <button
      id="${id}"
      class="btn app">
      <img
        class="app-image"
        src="${image}"
        alt="${name}"
      >
      <span class="app-name">
        ${name}
      </span>
    </button>
  `
}
