import $ from 'jquery';

export function eventHandler(images) {
  appEventHandler(images);
  // homeButton(onClickHome, _this)
}

export function appEventHandler(images) {
  $('.app').on({
    mouseenter: function (e) {
      const $elem = $(this);
      const id = $elem.attr('id');
      const key = `app-${id}-animated.gif`;
      attach($elem, key, images)
    },
    mouseleave: function (e) {
      const $elem = $(this);
      const id = $elem.attr('id');
      const key = `app-${id}.png`;
      attach($elem, key, images)
    }
  });
}

function homeButton(onClickHome, _this) {
  $('#home-button').on('click', () => {
    onClickHome(_this)
  })
}

function attach($elem, key, images) {
  const $child = $elem.find('img');
  const src = images[key].default;
  $child.attr('src', src)
}
