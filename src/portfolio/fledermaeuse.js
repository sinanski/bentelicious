function importAll(r) {
  return r.keys().map(r);
}

const images = importAll(require.context('../img/portfolio/fledermaeuse', false, /\.(png|jpe?g|svg)$/));

// console.log(images)
