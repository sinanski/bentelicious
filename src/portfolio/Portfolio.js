import $ from 'jquery';
import Slider from '../slider';
import { render as renderHome } from './home/render';
import renderImageOverView from './image-overview';
import animation from './animation';
import images from './loadImages';

const initialState = {
  open: undefined,
  previewTimeline: {},
  isFullScreen: false,
  slider: {}
};

export default class Portfolio {
  constructor() {
    onLoadImages = onLoadImages.bind(this)
    this.state = initialState;
    this.images = images;
    this.slider = new Slider({onClose: () => this.onHomeClick()});
    this.render();
    attachEventHandlers(this)
  }
  onHomeClick() {
    if ( this.state.isFullScreen ) {
      // Esc triggered beide outs
      this.slider.unmount();
      this.setState({ isFullScreen: false })
    } else if ( this.state !== initialState ) {
      this.state.previewTimeline.timeScale(2).reverse();
      this.setState(initialState)
    }
  }
  onAppClick(id) {
    renderImageOverView(this.images[id])
    const previewTimeline = animation.openPreview(id, onLoadImages);
    this.setState({
      open: id,
      previewTimeline,
    })
    previewTimeline.timeScale(1.5).play()
  }
  onImageClick(id) {
    const { images, state, slider } = this;
    slider.render(images[state.open], id)
    this.setState({isFullScreen: true})
  }
  setState(newState) {
    this.state = {
      ...this.state,
      ...newState
    }
  }
  render() {
    renderHome(this.onHomeClick, this);
  }
}

function onLoadImages() {
  const imageList = this.images[this.state.open]
  const $imageContainer = $('#image-preview');
  $imageContainer.children().each((i, elem) => {
    const $container = $(elem);
    const imageKey = Object.keys(imageList)[i];
    const image = `
      <img id="${imageKey}" src="${imageList[imageKey].default}" alt="${imageKey}" />
    `
    $container.append(image)
  })
  attachImageEventHandlers(this)
}



function attachImageEventHandlers(_this) {
  $('.image-preview__img').on('click', e => {
    const id = $(e.target).attr('id')
    _this.onImageClick(id)
  })
}

function attachEventHandlers(_this) {
  $('.app').on('click', e => {
    const $elem = $(e.target);
    const id = $elem.attr('id') || $elem.parent().attr('id')
    _this.onAppClick(id)
  })
  $('#home-button').on('click', e => {
    _this.onHomeClick()
  })
  $('body').on('keyup', e => {
    if(e.key === "Escape") {
      _this.onHomeClick()
    }
  })
}
